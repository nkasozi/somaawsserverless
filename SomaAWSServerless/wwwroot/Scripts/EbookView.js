﻿

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0].toUpperCase() === sParam.toUpperCase()) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function GoToLink(link) {
    var scrollbar_position = $(document).scrollTop();
    var bookmark_location = getUrlParameter('ebook') + "_" + getUrlParameter('chapter') + "_" + scrollbar_position;
    var full_url = "";
    if (link.indexOf('?') !== -1) {
        full_url = link + "&ChapterScrollPos=" + bookmark_location;
    }
    else {
        full_url = link + "?ChapterScrollPos=" + bookmark_location;
    }
    window.location = full_url;
}

