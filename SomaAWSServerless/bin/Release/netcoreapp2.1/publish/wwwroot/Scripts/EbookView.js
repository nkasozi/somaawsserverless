﻿var handled = false;
var LinkClicked = '';
$(document).ready(function () {

    setEventListeners();

   

    //window.history.pushState({ page: 1 }, "", "");

    //window.onbeforeunload = function () {
    //    //debugger;
    //    if (handled) { return; }
    //    handled = true;
    //    BookmarkChapterAndRedirect();
    //    return true;
    //}

    //window.onunload = function () {
    //    //debugger;
    //    if (handled) { return; }
    //    handled = true;
    //    BookmarkChapterAndRedirect();
    //    return true;
    //}


    //window.onpopstate = function (event) {
    //    //debugger;
    //    // "event" object seems to contain value only when the back button is clicked
    //    // and if the pop state event fires due to clicks on a button
    //    // or a link it comes up as "undefined"

    //    if (event.type === "popstate") {
    //        // Code to handle back button or prevent from navigation
    //        if (handled) { return; }

    //        handled = true;
    //        BookmarkChapter();
    //        if (window.confirm('Current location bookmarked')) {
    //            //debugger;
    //            window.history.back(1);
    //        }
    //    }
    //}



    //$(document).keydown(function (e) {
    //    switch (e.which) {
    //        case 37: // left arrow key
    //            $('#prev-chapter').click();
    //            e.preventDefault(); // prevent the default action (scroll / move caret)
    //            break;

    //        case 39: // right arrow key
    //            $('#next-chapter').click();
    //            e.preventDefault(); // prevent the default action (scroll / move caret)
    //            break;

    //        default: return; // exit this handler for other keys
    //    }

    //});
});

function checkLoginStatus() {
    var Status = localStorage.getItem("currentUser");
    if (Status === null) {
        window.location.href = "/Login";
    }
}

function BookmarkChapter() {
    //debugger;
    var scrollPos = $(document).scrollTop();
    var chapterID = getUrlParameter('chapter');
    var ebookID = getUrlParameter('ebook');
    var bookmark_url = "/BookMark?Ebook=" + ebookID + "&Chapter=" + chapterID + "&ChapterScrollPos=" + scrollPos;
    $.ajax({
        url: bookmark_url,
        async: false,
        success: function (data) {
            //debugger
        }

    });
}


var NextChapterHtml = "N/A";
var PrevChapterHtml = "N/A";

function GetNextChaptersAsync() {

    var next_chapter_link = $('#next-link').attr("href");
    var prev_chapter_link = $('#prev-link').attr("href");

    $.ajax({
        url: next_chapter_link,
        type: 'GET',
        success: function (data) {
            //debugger;
            var chapter_html = data.substring(data.indexOf("<body>") + 6, data.indexOf("</body>"));;
            // select the `notbody` tag and log for testing
            console.log(chapter_html);
            NextChapterHtml = chapter_html;
            BookmarkChapter();
        }

    });

    $.ajax({
        url: prev_chapter_link,
        type: 'GET',
        success: function (data) {
            //debugger;
            var chapter_html = data.substring(data.indexOf("<body>") + 6, data.indexOf("</body>"));;
            // select the `notbody` tag and log for testing
            console.log(chapter_html);
            PrevChapterHtml = chapter_html;
            BookmarkChapter();
        }

    });
}

function BookmarkChapterAndRedirect() {
    //debugger;
    var scrollPos = $(document).scrollTop();
    var chapterID = getUrlParameter('chapter');
    var ebookID = getUrlParameter('ebook');
    var bookmark_url = "/BookMark?Ebook=" + ebookID + "&Chapter=" + chapterID + "&ChapterScrollPos=" + scrollPos;
    $.ajax({
        url: bookmark_url,
        async: false,
        beforeSend: function () {
            return confirm("Bookmark Current location?");
        },
        success: function (data) {
            return confirm("Bookmark Current location?");
            


        }

    });
}

function BookmarkChapterAndAlert() {
    //debugger;
    var scrollPos = $(document).scrollTop();
    var chapterID = getUrlParameter('chapter');
    var ebookID = getUrlParameter('ebook');
    var bookmark_url = "/BookMark?Ebook=" + ebookID + "&Chapter=" + chapterID + "&ChapterScrollPos=" + scrollPos;
    $.ajax({
        url: bookmark_url,
        async: false,
        success: function (data) {

            $(document).scrollTop(scrollPos);
            alert('Current Location has been bookmarked');
        }

    });
    return true;
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0].toUpperCase() === sParam.toUpperCase()) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function setEventListeners() {

    //on log out
    $('#logout_link').click(function (e) {
        e.preventDefault();
        BookmarkChapter();
        window.location.href = "/Logout";
    });

    //on next chapter btn click
    $('#next-chapter').on('click', function (e) {
        LoadNextChapter();
    });

    //on go to library link click
    $('#library-link').on('click', function (e) {
        //debugger;
        GoToLibrary(e);
    });

    //on go to library btn click
    $('#library-btn').on('click', function (e) {
        //debugger;
        GoToLibrary(e);
    });

    //on prev btn click
    $('#prev-chapter').on('click', function (e) {
        LoadPrevChapter();
    });

    //on next link click
    $('#next-link').on('click', function (e) {
        LoadNextChapter(e);
    });

    $('#prev-link').on('click', function (e) {
        LoadPrevChapter(e);
    });

    $('#bookmark-link').on('click', function (e) {
        handleBookMarkLocationClick(e);
    });

    $('#bookmark-btn').on('click', function (e) {
        handleBookMarkLocationClick(e);
    });

    setInterval(GetNextChaptersAsync(),5000);

}

function GoToLibrary(e) {
    LinkClicked = 'library';
    e.preventDefault();
    BookmarkChapter();
    var chapter_link = '/EbookLibrary';
    window.location.href = chapter_link;
}

function handleBookMarkLocationClick(e)
{
    //debugger;
    e.preventDefault();
    $('#bookmark-btn').attr("disabled", "disabled");
    BookmarkChapterAndAlert();
    $('#bookmark-btn').removeAttr("disabled");
}

function LoadPrevChapter(e) {
    //debugger;
    LinkClicked = 'prev';
    e.preventDefault();
    BookmarkChapter();

    if (PrevChapterHtml === 'N/A') {
        var chapter_link = $('#prev-link').attr("href");
        window.location.href = chapter_link;
    }
    else {
        $('body').html(PrevChapterHtml);
        NextChapterHtml = 'N/A';
        PrevChapterHtml = 'N/A';
        GetNextChaptersAsync();
    }
}

function LoadNextChapter(e) {
    //debugger;
    LinkClicked = 'next';
    e.preventDefault();
    BookmarkChapter();
    if (NextChapterHtml === 'N/A') {
        var chapter_link = $('#next-link').attr("href");
        window.location.href = chapter_link;
    }
    else {
        $('body').html(NextChapterHtml);
        NextChapterHtml = 'N/A';
        PrevChapterHtml = 'N/A';
        GetNextChaptersAsync();
    }
}
