/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */

function goTo(link) {
    window.location.href = link;
}

function handleException(request, message,
    error) {
    var msg = "";
    msg += "Code: " + request.status + "\n";
    msg += "Text: " + request.statusText + "\n";
    if (request.responseJSON != null) {
        msg += "EXCEPTION: " +
            request.responseJSON.Message + "\n";
    }
    console.log(msg);
}

function openfileDialog() {
    $('#FileUploadControl').click();
    $('#btn-start-upload').removeClass('hidden');
    $('#btn-cancel-upload').removeClass('hidden');
    $('#progress-bar').removeClass('hidden');
}

var IsListViewShown = false;

function showListView() {
    if (IsListViewShown) {
        $('#coverflowView').show();
        $('#listView').hide();
        IsListViewShown = false;
    }
    else {
        $('#coverflowView').hide();
        $('#listView').show();
        IsListViewShown = true;
    }
}



function startUp() {

    //on log out
    $('#logout_link').click(function (e) {
        e.preventDefault();
        try {
            $.removeCookie('Token', { path: '/' });
        }
        catch (ex) {
        }
        window.location.href = "Logout";
    });

    $('#listView').hide();
    if ($.fn.reflect) {
        $('.photos .cover').reflect();
    }

    //debugger;
    var count = $('#main-content').attr("data-count") === undefined ? 0 : $('#main-content').attr("data-count");
    var middlecount = parseInt(count / 2) === NaN ? 1 : parseInt(count / 2);

    $('.photos').coverflow({
        easing: 'easeOutElastic',
        duration: 1000,
        index: middlecount,
        width: 320,
        height: 240,
        visible: 'density',
        density: 2,
        innerOffset: 50,
        innerScale: .7,

        selectedCss: { opacity: 1 },
        outerCss: { opacity: .1 },



        before: function () {
            $('#photos-name').stop(true).fadeOut('fast');
        },

        select: function (event, cover) {
            var img = $(cover).children().andSelf().filter('img').last();
            console.log(img);
            $('#photos-name').text(img.data('name') || 'unknown').stop(true).fadeIn('fast');
        },
    });


}

//first function called
$(function () {

    'use strict';

    startUp();

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'Upload',

        done: function (event, response) {
            //debugger;
            console.log(response.result);
            if (response.result.success) {
                window.location.href = 'EbookLibrary?UploadStatus=SUCCESS';
            }
            else {
                window.location.href = 'EbookLibrary?UploadStatus=FAILED&Reason=' + response.result.error;
            }
        }

    });


});
