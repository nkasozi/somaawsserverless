﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SomaWebApi.Models
{
    public class LendRequest
    {
        public string CurrentOwnerId { get; set; }
        public string NewReaderId { get; set; }
        public int EbookId { get; set; }
    }
}