﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json.Linq;
using SomaEbookManager.EntityClasses;

namespace FineUploader
{
    /// <remarks>
    /// Docs at https://github.com/Widen/fine-uploader/blob/master/server/readme.md
    /// </remarks>
    public class UploadResult : IActionResult
    {
       
    public const string ResponseContentType = "text/plain";


        private readonly bool _success;
        private readonly string _error;
        private readonly bool? _preventRetry;
        private readonly JObject _otherData;


        public UploadResult(bool success, object otherData = null, string error = null, bool? preventRetry = null)
        {
            _success = success;
            _error = error;
            _preventRetry = preventRetry;


            if (otherData != null)
                _otherData = JObject.FromObject(otherData);
        }


        public async Task ExecuteResultAsync(ActionContext context)
        { 
            var response = context.HttpContext.Response;
            response.ContentType = ResponseContentType;

            await  HttpResponseWritingExtensions.WriteAsync(response, BuildResponse());
          
        }


        public string BuildResponse()
        {
            var response = _otherData ?? new JObject();
            response["success"] = _success;


            if (!string.IsNullOrWhiteSpace(_error))
                response["error"] = _error;


            if (_preventRetry.HasValue)
                response["preventRetry"] = _preventRetry.Value;


            return response.ToString();
        }
    }

   

    public class UploadFileResult
    {
        public string name { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string deleteUrl { get; set; }
        public string thumbnailUrl { get; set; }
        public string deleteType { get; set; }
        public string StatusCode { get; set; }
        public string StatusDesc { get; set; }

        public static UploadFileResult GetUploadResult(SomaEbook somaEbook)
        {
            String getType = GetMimeMapping(somaEbook.FilePath);
            Console.WriteLine("UPLOAD STATUS: " + somaEbook?.StatusDesc);
            var result = new UploadFileResult()
            {
                name = somaEbook.Title,
                size = GetFileSize(somaEbook.FilePath),
                type = getType,
                url = null,
                deleteUrl = null,
                thumbnailUrl = $"Images/GetThumbnail/{somaEbook.CoverImageID}",
                deleteType = null,
                StatusCode=somaEbook.StatusCode,
                StatusDesc=somaEbook.StatusDesc
            };
            return result;
        }

        private static string GetMimeMapping(string filePath)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if (!provider.TryGetContentType(filePath, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        private static int GetFileSize(string FilePath)
        {
            try
            {
                return (int)new FileInfo(FilePath).Length;
            }
            catch (Exception)
            {

               return 0;
            }
            
        }
    }

    public class JsonFiles
    {
        public UploadFileResult[] files;
        public string TempFolder { get; set; }
        public JsonFiles(List<UploadFileResult> filesList)
        {
            files = new UploadFileResult[filesList.Count];
            for (int i = 0; i < filesList.Count; i++)
            {
                files[i] = filesList.ElementAt(i);
            }

        }
    }
}