﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SomaWebsite
{
    public class ImageResult : IActionResult

    {

        public ImageResult(Stream imageStream, string contentType)

        {
            this.ImageStream = imageStream ?? throw new ArgumentNullException("imageStream");

            this.ContentType = contentType ?? throw new ArgumentNullException("contentType");
        }



        public Stream ImageStream { get; private set; }

        public string ContentType { get; private set; }



        public async Task ExecuteResultAsync(ActionContext context)
        {
            try
            {
                if (context == null)
                    throw new ArgumentNullException("context");

                HttpResponse response = context.HttpContext.Response;

                response.ContentType = this.ContentType;
                byte[] buffer = new byte[4096];

                while (true)
                {

                    int read = this.ImageStream.Read(buffer, 0, buffer.Length);

                    if (read == 0)
                        break;

                    await response.Body.WriteAsync(buffer, 0, read);

                }


            }
            catch (Exception)
            {

            }
        }
    }
}