﻿using SomaEbookManager.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SomaWebsite.Models
{
    public class MyLendRequest:LendRequest
    {
        public IEnumerable<SomaEbook> Items { get; set; }
    }
}