﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SomaEbookManager.EntityClasses;
using SomaWebsite;
using Microsoft.AspNetCore.Http.Extensions;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.AspNetCore.Hosting;

namespace SomaAWSServerless.Controllers
{
    [Route("Images")]
    public class ImagesController : Controller
    {
        private readonly IHostingEnvironment environment;


        public ImagesController(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        private string MapPath(string itemName)
        {
            return Path
                   .Combine(
                     environment.WebRootPath,
                     itemName.ReplaceFirst("~/", string.Empty).ReplaceFirst("/", string.Empty)
                     );
        }

        // GET: Images
        public IActionResult Index()
        {
            return GetImage("");
        }

        // GET: Images/23627377929
        public IActionResult Index(string ImageID)
        {
            return GetImage(ImageID);
        }

        // GET: Images/GetImage/5
        [Route("GetThumbnail/{ImageID}")]
        [HttpGet()]
        public IActionResult GetThumbnail(string ImageID)
        {
            try
            {
                //because MVC is failing to pick up the variable
                //I had to do this
                string rawurl = Request.GetDisplayUrl();
                ImageID = rawurl.Split('/').LastOrDefault() ?? "";

                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
                SomaEbookImage image = ReadImageFromFolder(ImageID);

                if (image.StatusCode == Globals.SUCCESS_STATUS_CODE)
                {
                    //handle failure
                    return ImageActionResult(image);
                }

                image = soma.GetEbookImage(ImageID);
                if (image.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    //handle failure
                    return new StatusCodeResult(404);
                }

                byte[] data = image.Image;//System.IO.File.ReadAllBytes(@"D:\Music\New folder\placeholder.png");
                string mimeType = $"image/{Path.GetExtension(image.ImageName).Replace(".", string.Empty)}";


                byte[] resizedImage;
                using (Image orginalImage = Image.FromStream(new MemoryStream(data)))
                {
                    ImageFormat orginalImageFormat = orginalImage.RawFormat;
                    int orginalImageWidth = orginalImage.Width;
                    int orginalImageHeight = orginalImage.Height;
                    int resizedImageWidth = 100; // Type here the width you want
                    int resizedImageHeight = Convert.ToInt32(resizedImageWidth * orginalImageHeight / orginalImageWidth);
                    using (Bitmap bitmapResized = new Bitmap(orginalImage, resizedImageWidth, resizedImageHeight))
                    {
                        using (MemoryStream streamResized = new MemoryStream())
                        {
                            bitmapResized.Save(streamResized, orginalImageFormat);
                            resizedImage = streamResized.ToArray();
                        }
                    }
                }
                return new ImageResult(new MemoryStream(resizedImage), mimeType);
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }


        // GET: Images/GetImage/5
        [Route("GetImage/{ImageID}")]
        [HttpGet()]
        public IActionResult GetImage(string ImageID)
        {
            try
            {
                //because MVC is failing to pick up the variable
                //I had to do this
                string rawurl = Request.GetDisplayUrl();
                ImageID = rawurl.Split('/').LastOrDefault() ?? "";

                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
                SomaEbookImage image = ReadImageFromFolder(ImageID);

                if (image.StatusCode == Globals.SUCCESS_STATUS_CODE)
                {
                    //handle failure
                    return ImageActionResult(image);
                }

                image = soma.GetEbookImage(ImageID);

                if (image.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    //handle failure
                    return new StatusCodeResult(404);
                }

                return ImageActionResult(image);

            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        private SomaEbookImage ReadImageFromFolder(string imageID)
        {
            SomaEbookImage image = new SomaEbookImage();
            string folder_path = MapPath("~/Images");

            //folder not found..we cant continue
            if (string.IsNullOrEmpty(folder_path))
            {
                image.StatusCode = Globals.FAILURE_STATUS_CODE;
                image.StatusDesc = "Images Folder Not Found";
                return image;
            }

            string[] filenames = Directory.GetFiles(folder_path);
            string fi = Path.GetFileName(filenames[0]);

            string filename = filenames.
                              Where(i => Path.GetFileName(i.ToUpper()) == imageID.ToUpper()).
                              FirstOrDefault();

            if (string.IsNullOrEmpty(filename))
            {
                image.StatusCode = Globals.FAILURE_STATUS_CODE;
                image.StatusDesc = "Image Not Found";
                return image;
            }

            image.Image = System.IO.File.ReadAllBytes(filename);
            image.ImageFilePath = filename;
            image.ImageID = imageID;
            image.ImageName = Path.GetFileName(filename);
            image.IdRef = imageID;
            image.StatusCode = Globals.SUCCESS_STATUS_CODE;
            image.StatusDesc = Globals.SUCCESS_TEXT;
            return image;
        }

        private IActionResult ImageActionResult(SomaEbookImage image)
        {
            byte[] data = image.Image;
            string mimeType = string.IsNullOrEmpty(Path.GetExtension(image.ImageFilePath)) ? "image/png" : $"image/{Path.GetExtension(image.ImageFilePath).Replace(".",string.Empty)}";
            Response.ContentLength = data.Length;
            return File(data, mimeType);
        }
    }
}
