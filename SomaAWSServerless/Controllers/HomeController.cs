﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FineUploader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SomaEbookManager;
using SomaEbookManager.EntityClasses;
using SomaWebsite.Models;

namespace SomaAWSServerless
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment environment;


        public HomeController(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        private string MapPath(string itemName)
        {
            return Path
                   .Combine(
                     environment.WebRootPath,
                     itemName.ReplaceFirst("~/", string.Empty).ReplaceFirst("/", string.Empty)
                     );
        }

        [Route("/login")]
        [HttpGet]
        public IActionResult Login()
        {
            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                ViewBag.ErrorMsg = "YOUR SESSION HAS EXPIRED";
                return View("Login");
            }

            return EbookLibrary();
        }

        [Route("/QuickLogin")]
        [HttpGet]
        public IActionResult QuickLogin()
        {
            return View("SSO_QuickLogin");
        }

        [Route("/SSO_QuickLogin")]
        [HttpGet]
        public IActionResult SSO_QuickLogin()
        {
            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                return RedirectToAction("EbookLibrary");
            }

            return View("SSO_QuickLogin");
        }

        [Route("/SSO_QuickLogin")]
        [HttpPost]
        public IActionResult SSO_QuickLogin(string name, string email)
        {
            SomaSystemUser user = GetSomaSystemUserFromSSO();

            //we have not saved him successfully
            if (user.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                //error on login
                ViewBag.ErrorMsg = user.StatusDesc;

                //take his ass back
                return View("SSO_QuickLogin");
            }

            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();

            SomaSystemUser systemUser = soma.GetUserByID(user.Username);

            //user already exists
            if (systemUser.StatusCode == Globals.SUCCESS_STATUS_CODE)
            {
                //log him in
                SessionToken user_token = soma.Login(systemUser.Username, systemUser.Password);

                //set session stuff
                SetSessionValues(user_token);

                //show him his library
                return EbookLibrary(systemUser);
            }

            //user doesnt exist...so we need to create them
            //save the new user
            SomaEbookManager.Status result = soma.SaveSystemUser(user);

            //we have failed to save him successfully
            if (result.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                //error on login
                ViewBag.ErrorMsg = result.StatusDesc;

                //take his ass back
                return View("SSO_QuickLogin");
            }

            //we have created the user succcessfully
            SessionToken token = soma.Login(user.Username, user.Password);

            //set session stuff
            SetSessionValues(token);

            //show him his library
            return EbookLibrary(user);
        }

        private SomaSystemUser GetSomaSystemUserFromSSO()
        {
            SomaSystemUser systemUser = new SomaSystemUser();
            try
            {
                // Your Site Settings
                String site_subdomain = "thereadersbayorg";
                String site_public_key = "5ef875b5-8d44-43d6-8ab9-f64161fd0acb";
                String site_private_key = "c454f054-b6fb-48b3-8fe8-b0d73dda6287";
                string connection_token = Request.Form["connection_token"];

                if (string.IsNullOrEmpty(connection_token))
                {
                    systemUser.StatusCode = Globals.FAILURE_STATUS_CODE;
                    systemUser.StatusDesc = "No Connection Token Found";
                    return systemUser;
                }

                // API Access Domain
                String site_domain = site_subdomain + ".api.oneall.com";

                // Connection Resource
                string resource_uri = $"https://{site_domain}/connections/{connection_token}.json";


                // Forge authentication string username:password
                String site_authentication = site_public_key + ":" + site_private_key;
                String encoded_site_authentication = Convert.ToBase64String(Encoding.ASCII.GetBytes(site_authentication.Replace("[\n\r]", string.Empty)));

                // Setup connection
                HttpWebRequest connection = WebRequest.Create(resource_uri) as HttpWebRequest;

                // Connect using basic auth
                connection.Method = "GET";
                connection.Headers.Add("Authorization", "Basic " + encoded_site_authentication);
                connection.Timeout = 10000;
                var response = (HttpWebResponse)connection.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Console.WriteLine(responseString);


                dynamic data = JObject.Parse(responseString);
                dynamic user = data?.response?.result?.data?.user?.identity ?? data?.response?.result?.data?.user?.Identity;
                dynamic status = data?.response?.result?.status;

                if (status?.flag?.ToString().ToUpper() != "SUCCESS")
                {
                    string status_desc = status?.info;
                    systemUser.StatusCode = Globals.FAILURE_STATUS_CODE;
                    systemUser.StatusDesc = status_desc;
                    return systemUser;
                }

                string preferredUsername = user?.preferredUsername;
                string email = (user?.emails as JArray).FirstOrDefault()?.ToString();
                email = email ?? "";
                Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);

                //find items that matches with our pattern
                MatchCollection emailMatches = emailRegex.Matches(email);

                email = emailMatches.Count > 0 ? emailMatches.FirstOrDefault().Value : email;

                systemUser.Email = email;
                systemUser.Username = email;//preferredUsername ?? systemUser.Email;

                if (string.IsNullOrEmpty(email))
                {
                    systemUser.StatusCode = Globals.FAILURE_STATUS_CODE;
                    systemUser.StatusDesc = "No Email Found. An email is required for login";
                    return systemUser;
                }

                systemUser.Password = SharedCommons.GenerateRandomString();
                systemUser.StatusCode = Globals.SUCCESS_STATUS_CODE;
                systemUser.StatusDesc = Globals.SUCCESS_TEXT;
                return systemUser;
            }
            catch (Exception ex)
            {
                systemUser.StatusCode = Globals.FAILURE_STATUS_CODE;
                systemUser.StatusDesc = $"ERROR:{ex.Message}";
                return systemUser;
            }

        }

        [Route("/QuickLogin")]
        [HttpPost]
        public IActionResult QuickLogin(string name, string email)
        {
            SomaSystemUser user = new SomaSystemUser() { Username = email, Password = SomaSharedCommons.GeneratePassword() };
            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();

            //save the new user
            SomaEbookManager.Status result = soma.SaveSystemUser(user);

            //we have saved him successfully
            if (result.StatusCode == Globals.SUCCESS_STATUS_CODE)
            {
                SessionToken token = soma.Login(user.Username, user.Password);

                //set session stuff
                SetSessionValues(token);

                //show him his library
                return EbookLibrary();
            }
            else
            {
                //error on login
                ViewBag.ErrorMsg = result.StatusDesc;

                //take his ass back
                return View("QuickLogin");
            }

        }

        [Route("/logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                ViewBag.ErrorMsg = "YOU HAVE BEEN LOGGED OUT";
                return RedirectToAction("SSO_QuickLogin");
            }

            //get bookmark data
            string data = Request?.Query["ChapterScrollPos"];

            //contains data for bookmarking
            if (!string.IsNullOrEmpty(data))
            {
                BookMarkPositionInChapter(data, sessionUser);
            }

            //end the session
            TerminateSession();

            //go to login page
            return RedirectToAction("SSO_QuickLogin");

        }

        private void TerminateSession()
        {
            try
            {
                if (Request.Cookies["Token"] != null)
                {
                    string cookie_name = "Token";
                    string cookie_value = "";
                    Response.Cookies.Append(
                        cookie_name,
                        cookie_value,
                        new CookieOptions()
                        {
                            Expires = DateTime.Now.AddDays(-1),
                            IsEssential = true
                        }
                    );
                }
            }
            catch { }
        }

        [Route("/login")]
        [HttpPost]
        public IActionResult Login(SomaSystemUser user)
        {


            //log user in
            SomaEbookManagerAPI api = new SomaEbookManagerAPI();
            SessionToken token = api.Login(user.Username, user.Password);

            //login was ok
            if (token.StatusCode == Globals.SUCCESS_STATUS_CODE)
            {
                //set session stuff
                SetSessionValues(token);

                //show him his library
                return EbookLibrary(user);
            }
            else
            {
                //error on login
                ViewBag.ErrorMsg = "ERROR:" + token.StatusDesc;

                //take his ass back
                return View("Login");
            }
        }

        [Route("/EbookLibrary")]
        public IActionResult EbookLibrary(SomaSystemUser sessionUser = null)
        {
            //check for session
            sessionUser = string.IsNullOrEmpty(sessionUser.Username) ? CheckForSessionUser() : sessionUser;

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                ViewBag.ErrorMsg = "YOUR SESSION HAS EXPIRED";
                return QuickLogin();
            }

            //get bookmark data
            string data = Request?.Query["ChapterScrollPos"];

            //contains data for bookmarking
            if (!string.IsNullOrEmpty(data))
            {
                BookMarkPositionInChapter(data, sessionUser);
            }

            //get ebooks for the user
            //string imagesFolder = Server.MapPath("~/Images");
            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
            List<SomaEbook> ebooks = soma.GetAllUsersEbooks(sessionUser.Username);

            //Upload Sample Ebooks
            if (ebooks.Count == 0)
            {
                PreLoadNewUserLibrary(sessionUser, soma);
                ebooks = soma.GetAllUsersEbooks(sessionUser.Username);
            }

            //return response
            ViewBag.Ebooks = ebooks;
            ViewBag.EbookCount = ebooks.Count;
            ViewBag.Username = sessionUser.Username;

            string upload_status = Request.Query["UploadStatus"];
            string successMsg = "Books have been Uploaded Successfully. They will show up shortly";

            if (!string.IsNullOrEmpty(upload_status))
                ViewBag.Msg = upload_status == Globals.SUCCESS_TEXT ? successMsg : Request.Query["Reason"].ToString();

            return GetEbookLibraryView();
        }

        //upload all ebooks in the samples folder
        private void PreLoadNewUserLibrary(SomaSystemUser sessionUser, SomaEbookManagerAPI soma)
        {
            string folder_path = MapPath("~/SampleBooks");

            //folder not found..we cant continue
            if (string.IsNullOrEmpty(folder_path))
            {
                return;
            }

            List<string> ebooks_filepaths = new List<string>();

            string[] filenames = Directory.GetFiles(folder_path);

            foreach (var ebook_file_name in filenames)
            {
                ebooks_filepaths.Add(ebook_file_name);
            }

            soma.ExtractAndSaveEbooks(sessionUser.Username, ebooks_filepaths.ToArray());

        }

        private IActionResult GetEbookLibraryView(object model = null)
        {
            if (IsOperaMiniBrowser())
            {
                ViewBag.MobileCssClass = "zoom zoom-moz";
                ViewBag.IsMobile = true;
                return View("EbookLibrary", model);
                //return View("MobileEbookLibrary", model);
            }
            else
            {
                ViewBag.MobileCssClass = "";
                ViewBag.IsMobile = false;
                return View("EbookLibrary", model);
            }
        }

        [Route("/Images/{ImageID}")]
        [HttpGet]
        public IActionResult LoadImage(string ImageID)
        {
            Controllers.ImagesController imagesController = new Controllers.ImagesController(environment);
            return imagesController.GetImage(ImageID);
        }

        [Route("/Images/GetImages/{ImageID}")]
        [HttpGet]
        public IActionResult GetImage(string ImageID)
        {
            Controllers.ImagesController imagesController = new Controllers.ImagesController(environment);
            return imagesController.GetImage(ImageID);
        }

        [Route("/BookMark")]
        public IActionResult BookMark(string EbookId = null, string ChapterId = null, string ChapterScrollPos = null, SomaSystemUser sessionUser = null)
        {
            //check for session
            sessionUser = sessionUser ?? CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                //forbidden
                return new StatusCodeResult(403);
            }

            //get browser details
            bool IsMobileBrowser = Request.IsMobileBrowser();


            //read parameters
            string ebookId = EbookId ?? Request?.Query["ebook"];
            string chapterId = ChapterId ?? Request?.Query["chapter"];
            string scrollbarPosition = ChapterScrollPos ?? Request?.Query["ChapterScrollPos"];
            scrollbarPosition = ParseInt(scrollbarPosition);

            //its done
            if (scrollbarPosition == "0")
            {
                return Json("success:true");
            }

            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
            SomaEbookManager.Status bookmark_result = soma.BookMarkEbookChapter(sessionUser.Username, ebookId, chapterId, scrollbarPosition);

            //bookmarking is not a critical functionality
            //hence why we ignore the status returned
            return Json("success:true");
        }

        private static string ParseInt(string scrollbarPosition)
        {
            try
            {
                return int.Parse(scrollbarPosition.Split('.')[0]).ToString();
            }
            catch
            {
                return "0";

            }
        }

        [Route("/EbookView")]
        public IActionResult EbookView()
        {


            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                ViewBag.ErrorMsg = "YOUR SESSION HAS EXPIRED";
                return QuickLogin();
            }

            //get bookmark data
            string data = Request?.Query["ChapterScrollPos"];

            //contains data for bookmarking
            if (!string.IsNullOrEmpty(data))
            {
                BookMarkPositionInChapter(data, sessionUser);
            }

            //get browser details
            bool IsMobileBrowser = Request.IsMobileBrowser();

            //read parameters
            string ebookId = Request?.Query["ebook"];
            string chapterId = Request?.Query["chapter"];
            string ebookTitle = Request?.Query["ebookTitle"];

            //get chapter 
            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
            SomaEbookChapter chapter = soma.GetEbookChapterByChapterIndex_Internal(sessionUser.Username, ebookId, chapterId);
            chapter.ChapterContent = chapter.ReplaceImagePathsToWebPaths(Url.Action("Index", "Images"));

            //get chapter was not ok
            if (chapter.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                return RedirectToAction("EbookLibrary", new { UploadStatus = "FAILED", Reason = chapter.StatusDesc });
            }

            //book mark chapter
            Task.Factory.StartNew(() => soma.BookMarkEbookChapter(sessionUser.Username, ebookId, chapterId));

            //return result
            ViewBag.Username = sessionUser.Username;
            ViewBag.ChapterContent = chapter.ChapterContent;
            ViewBag.ScrollbarPosition = chapter.ScrollbarPosition;
            string title = UrlEncoder.Default.Encode(ebookTitle.Replace("'", string.Empty));
            ViewBag.NextChapter = $"?ebook={ebookId}&ebookTitle={title}&chapter=" + GetNextChapter(chapter, sessionUser.Username, true);
            ViewBag.PrevChapter = $"?ebook={ebookId}&ebookTitle={title}&chapter=" + GetNextChapter(chapter, sessionUser.Username, false);
            ViewBag.CurrentChapter = $"?ebook={ebookId}&ebookTitle={title}&chapter=" + chapterId;
            ViewBag.EbookTitle = ebookTitle;

            return GetEbookViewDependingOnDevice();
        }

        private void BookMarkPositionInChapter(string data, SomaSystemUser user)
        {
            try
            {
                string ebooksId = data.Split('_')?[0];
                string ebookChapterId = data.Split('_')?[1];
                string chapterScrollPosition = data.Split('_')[2];
                Task.Factory.StartNew(() => BookMark(ebooksId, ebookChapterId, chapterScrollPosition, user));
            }
            catch
            {

            }
        }

        private ActionResult GetEbookViewDependingOnDevice()
        {
            if (IsOperaMiniBrowser())
            {
                return View("MobileEbookView");
            }
            else
            {
                return View("EbookView");
            }
        }

        private string GetNextChapter(SomaEbookChapter chapter, string username, bool IsNextChapter)
        {
            int proposed_next_chapter = IsNextChapter ? chapter.ChapterIndex_Internal + 1 : chapter.ChapterIndex_Internal - 1;
            try
            {
                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
                SomaEbookChapter ebook = soma.GetEbookChapterByChapterIndex_Internal(username, chapter.EbookId, proposed_next_chapter.ToString());

                if (ebook.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    return chapter.ChapterIndex_Internal.ToString();
                }

                return ebook.ChapterIndex_Internal.ToString();
            }
            catch (Exception)
            {
                return chapter.ChapterIndex_Internal.ToString();
            }
        }

        [Route("/Upload")]
        public IActionResult Upload()
        {
            List<UploadFileResult> uploadedFiles = new List<UploadFileResult>();
            try
            {
                ConcurrentBag<string> uploaded_files = new ConcurrentBag<string>();

                //get user logged in
                SomaSystemUser user = CheckForSessionUser();

                if (user.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    Console.WriteLine("ERROR: " + user.StatusDesc);
                    //weird error on upload
                    return new StatusCodeResult(403);
                }

                var folderPath = "/tmp";//MapPath("~/Temp");//environment.IsDevelopment() ? @"D:\Work\Temp\" : @"\tmp\";
                Console.WriteLine("FOLDER: " + folderPath);
                Globals.DIR_SEPARATOR = "/";

                SomaEbookManager.Status create_status = SomaSharedCommons.CreateFilePathIfNotExists(folderPath);

                if (create_status.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    Console.WriteLine("ERROR: " + create_status.StatusDesc);
                    //handle failure here
                    return new StatusCodeResult(500);
                }

                // Get the uploaded file from the Files collection
                Parallel.ForEach(Request.Form.Files, file =>
                {
                    if (file.Length <= 0)
                    {
                        return;
                    }

                    // Validate the uploaded image(optional)
                    var httpPostedFile = Request.Form.Files.Where(i => i.FileName == file.FileName).FirstOrDefault();


                    // Get the complete file path
                    var fileSavePath = Path.Combine(folderPath, httpPostedFile.FileName);
                    Console.WriteLine("filepath: " + fileSavePath);

                    // Save the uploaded file to "UploadedFiles" folder
                    using (var fileStream = new FileStream(fileSavePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    Console.WriteLine("file saved: " + fileSavePath);

                    uploaded_files.Add(fileSavePath);
                });


                //extract and save ebook
                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
                List<SomaEbook> uploaded_books = soma.ExtractAndSaveEbooks(user.Username, uploaded_files.ToArray());

                SomaEbook uploaded_book = uploaded_books.FirstOrDefault();

                //hack
                if (uploaded_book != null)
                {
                    UploadResult uploadResult = uploaded_book.StatusCode == Globals.SUCCESS_STATUS_CODE ? new UploadResult(true) : new UploadResult(false, null, uploaded_book.StatusDesc, null);
                    return uploadResult;
                }

                uploaded_books.ForEach(i =>
                {
                    UploadFileResult fileResult = UploadFileResult.GetUploadResult(i);
                    uploadedFiles.Add(fileResult);
                });

                JsonFiles jsonFiles = new JsonFiles(uploadedFiles);
                return Json(jsonFiles);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                Console.WriteLine("STACK_TRACE: " + ex.StackTrace);
                UploadResult uploadResult = new UploadResult(false, null, $"Error:{ex.Message}", null);
                return uploadResult;
            }

        }

        [Route("/LendEbook")]
        [HttpGet]
        public IActionResult LendEbook()
        {
            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                ViewBag.ErrorMsg = "YOUR SESSION HAS EXPIRED";
                return QuickLogin();
            }

            //get ebooks for the user
            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
            List<SomaEbook> ebooks = soma.GetAllUsersEbooks(sessionUser.Username);

            //return response
            ViewBag.Ebooks = ebooks;

            MyLendRequest myLendRequest = new MyLendRequest
            {
                EbookID = Request.Query["ebook"],
                Items = ShortenBookTitles(ebooks),
                DurationInHours = "72"
            };

            ViewBag.Username = sessionUser.Username;
            ViewBag.IsMobile = IsOperaMiniBrowser();
            ViewBag.MobileCssClass = IsOperaMiniBrowser() ? "zoom zoom-moz" : "";

            return View("LendEbookView", myLendRequest);
        }

        private List<SomaEbook> ShortenBookTitles(List<SomaEbook> ebooks)
        {
            foreach (var ebook in ebooks)
            {
                string title = ebook.Title;
                string[] title_parts = title.Split(' ');
                string new_title = "";
                for (int i = 0; i < title_parts.Length; i++)
                {
                    if (i % 4 == 0)
                    {
                        new_title += title_parts[i] + Environment.NewLine;
                        continue;
                    }
                    new_title += title_parts[i] + " ";
                }
                ebook.Title = new_title;
            }
            return ebooks;
        }


        [Route("/RemoveEbook")]
        [HttpGet]
        public async Task RemoveEbook()
        {
            Status result = new Status();
            try
            {
                //get user logged in
                SomaSystemUser owner = CheckForSessionUser();

                if (owner.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    //forbidden
                    Redirect($"{Url.Action("SSO_QuickLogin")}");
                }

                string ebookId = Request.Query["ebook"];

                //build expected parameters 
                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();

                //lend ebook
                result = soma.DeleteEbook(owner.Username, owner.Password, ebookId);

                if (result.StatusCode == Globals.SUCCESS_STATUS_CODE)
                {
                    Response.Redirect($"{Url.Action("EbookLibrary")}/?UploadStatus=FAILED&Reason={result.StatusDesc}");

                }

                Redirect($"{Url.Action("EbookLibrary")}/?UploadStatus=FAILED&Reason={result.StatusDesc}");
            }
            catch (Exception ex)
            {
                //weird error on lend
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"ERROR: {ex.Message}";
            }

            //return JSON
            Redirect($"{Url.Action("EbookLibrary")}/?UploadStatus=FAILED&Reason={result.StatusDesc}");
        }

        [Route("/LendEbook")]
        [HttpPost]
        public ActionResult LendEbook(MyLendRequest myLendRequest)
        {
            Status result = new SomaEbookManager.Status();
            try
            {
                //get user logged in
                SomaSystemUser owner = CheckForSessionUser();

                if (owner.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    //weird error on upload
                    return new StatusCodeResult(403);
                }

                myLendRequest.CurrentOwnerID = owner.Username;
                myLendRequest.CurrentOwnerPassword = owner.Password;

                //build expected parameters 
                SomaEbookManagerAPI soma = new SomaEbookManagerAPI();

                //lend ebook
                result = soma.LendEbookToOtherUser(myLendRequest);

                if (result.StatusCode == Globals.SUCCESS_STATUS_CODE)
                {
                    return Redirect($"EbookLibrary?UploadStatus=FAILED&Reason={result.StatusDesc}");

                }

                return Redirect($"EbookLibrary?UploadStatus=FAILED&Reason={result.StatusDesc}");
            }
            catch (Exception ex)
            {
                //weird error on lend
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"ERROR: {ex.Message}";
            }

            //return JSON
            return Redirect($"EbookLibrary?UploadStatus=FAILED&Reason={result.StatusDesc}");
        }

        private SomaSystemUser CheckForSessionUser(string atoken = null)
        {
            SomaSystemUser result = new SomaSystemUser();
            try
            {

                string token = string.IsNullOrEmpty(atoken) ?
                               Request.Cookies["Token"]?.ToString() :
                               atoken;
                SomaEbookManagerAPI managerAPI = new SomaEbookManagerAPI();
                result = managerAPI.GetUserByToken(token);
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return result;
        }

        private bool IsOperaMiniBrowser()
        {
            //to handle detection of firefox mobile
            if (Request.Headers["User-Agent"].ToString().ToUpper().Contains("MOBILE") && (Request.Headers["User-Agent"].ToString().ToUpper().Contains("FIREFOX")))
            {
                return true;
            }
            //all other mobile browsers like chrome and edge can be detected by this guy
            if (Request.IsMobileBrowser())
            {
                return true;
            }
            return false;
        }

        private void SetSessionValues(SessionToken token)
        {
            string cookie_name = "Token";
            string cookie_value = token.TokenValue;

            Response.Cookies.Append(
                cookie_name,
                cookie_value,
                new CookieOptions()
                {
                    Path = "/",
                    HttpOnly = false,
                    IsEssential = true, //<- there
                    Expires = DateTime.Now.AddYears(1)
                }
            );

            //added this because request will be redirected to ebook
            //library method immediately that method checks for this shit
            //HttpContext.Session.SetString("Token", token.TokenValue);
        }


        public IActionResult Index()
        {
            ViewBag.Title = "TheReadersBay";
            //check for session
            SomaSystemUser sessionUser = CheckForSessionUser();

            //its done
            if (sessionUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                return View("SSO_QuickLogin");
            }

            return EbookLibrary(sessionUser);
        }
    }
}