//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("DbEntityStandard")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyCopyrightAttribute("https://opensource.org/licenses/MIT")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("Simply better Stored Procedure Support for Castle ActiveRecord")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("16.0.0.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("16.0.0")]
[assembly: System.Reflection.AssemblyProductAttribute("DbEntityStandard")]
[assembly: System.Reflection.AssemblyTitleAttribute("DbEntityStandard")]
[assembly: System.Reflection.AssemblyVersionAttribute("16.0.0.0")]
[assembly: System.Resources.NeutralResourcesLanguageAttribute("en")]

// Generated by the MSBuild WriteCodeFragment class.

