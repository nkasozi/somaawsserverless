﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Globals
{
    public static string SUCCESS_STATUS_CODE = "0";
    public static string FAILURE_STATUS_CODE = "100";
    public static string VALIDATION_FAILURE_STATUS_CODE = "105";
    public static string SUCCESS_TEXT = "SUCCESS";
    public static string UNKNOWN = "UNKNOWN";
    public const string EPUB_FILE_EXTENSION = "EPUB";
    public static string DIR_SEPARATOR = @"\";

    public enum AUDIT_ACTIONS
    {
        LOG_IN,
        LOG_OUT,
        LEND_ACTION,
        UPDATE,
        CREATE,
        DELETE,
        RETRIEVE
    }
}

