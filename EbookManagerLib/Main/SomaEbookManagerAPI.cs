﻿using SomaEbookManager.EntityClasses;
using SomaEbookManager.HelperClasses;
using System.Collections.Generic;
using SomaEbookManager;

public interface IEbookManagerAPI
{
    //get an image by ID
    SomaEbookImage GetEbookImage(string imageID);

    //get an image by ID
    SessionToken Login(string userID, string Password);

    //get an image by ID
    SomaSystemUser GetUserByToken(string token);

    //get an image by ID
    SomaSystemUser GetUserByID(string username);

    //logs a user into the system
    SomaSystemUser VerifyUserCredentials(string userId, string Password);

    //logs the user out of the system
    Status LogOutSystemUser(SomaSystemUser user);

    //save System user details
    Status SaveSystemUser(SomaSystemUser user);

    //uploads ebooks selected by user for upload
    List<SomaEbook> ExtractAndSaveEbooks(string userID, params string[] filepaths_to_ebooks);

    //Fetches all Ebooks attached to the user that have not been lent out
    List<SomaEbook> GetAllUsersEbooks(string userID);

    //Gets a Specific Ebook owned by user if it is not lent out
    SomaEbook GetEbookById(string userID, string EbookId);

    //User can lend ebook to other user
    Status LendEbookToOtherUser(LendRequest lendRequest);

    //User can change ebook format
    SomaEbook ConvertEbookFileFormat(string ebookID, SomaEbookType toFileFormat);

    //Get chapter using the index defined within the original ebook
    SomaEbookChapter GetEbookChapterByChapterIndex_External(string userID, string ebookId, string chapterId);

    //Get chapter using the index assigned when extracting it from the original ebook
    SomaEbookChapter GetEbookChapterByChapterIndex_Internal(string userID, string ebookId, string chapterId);

    //book mark current ebook chapter
    Status BookMarkEbookChapter(string userID, string ebookId, string chapter_index, string chapterScrollPos = null);

    //delete an ebook from library
    Status DeleteEbook(string ownerID, string password, string EbookId);
}


public class SomaEbookManagerAPI : IEbookManagerAPI
{
    BussinessLogic bizHelper = new BussinessLogic();

    public SomaEbook ConvertEbookFileFormat(string ebookID, SomaEbookType toFileFormat)
    {
        return bizHelper.ConvertEbookFileFormat(ebookID, toFileFormat);
    }

    public List<SomaEbook> GetAllUsersEbooks(string userID)
    {
        return bizHelper.GetAllUsersEbooks(userID);
    }

    public SomaEbook GetEbookById(string userID, string EbookId)
    {
        return bizHelper.GetEbookByIDAndOwnerID(userID, EbookId);
    }

    public Status LendEbookToOtherUser(LendRequest lendRequest)
    {
        return bizHelper.LendEbookToOtherUser(lendRequest);
    }

    public Status DeleteEbook(string ownerID, string password, string EbookId)
    {
        return bizHelper.DeleteEbook(ownerID, password, EbookId);
    }

    public SomaSystemUser VerifyUserCredentials(string userId, string Password)
    {
        return bizHelper.VerifyUserCredentials(userId, Password);
    }

    public Status LogOutSystemUser(SomaSystemUser user)
    {
        return bizHelper.LogOutSystemUser(user);
    }

    public Status SaveSystemUser(SomaSystemUser user)
    {
        return bizHelper.SaveSystemUser(user);
    }

    public List<SomaEbook> ExtractAndSaveEbooks(string userID, params string[] filepaths_to_ebooks)
    {
        return bizHelper.ExtractAndSaveEbooks(userID, filepaths_to_ebooks);
    }

    public SomaEbookChapter GetEbookChapterByChapterIndex_External(string userID, string ebookId, string chapterId)
    {
        return bizHelper.GetEbookChapterByChapterIndex_External(userID, ebookId, chapterId);
    }

    public SomaEbookChapter GetEbookChapterByChapterIndex_Internal(string userID, string ebookId, string chapterId)
    {
        return bizHelper.GetEbookChapterByChapterIndex_Internal(userID, ebookId, chapterId);
    }

    public Status BookMarkEbookChapter(string userID, string ebookId, string chapter_index, string chapterScrollPos = null)
    {
        return bizHelper.BookMarkEbookChapter(userID, ebookId, chapter_index, chapterScrollPos);
    }

    public SomaEbookImage GetEbookImage(string imageID)
    {
        return bizHelper.GetEbookImage(imageID);
    }

    public SomaSystemUser GetUserByToken(string token)
    {
        return bizHelper.GetUserByToken(token);
    }

    public SomaSystemUser GetUserByID(string username)
    {
        return bizHelper.GetUserByUsername(username);
    }

    public SessionToken Login(string userID, string Password)
    {
        return bizHelper.Login(userID, Password);
    }
}
