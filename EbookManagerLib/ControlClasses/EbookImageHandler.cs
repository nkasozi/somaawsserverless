﻿using System;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using SomaEbookManager.EntityClasses;

namespace SomaEbookManager
{

    public class EbookImageHandler
    {
        private static readonly string BucketName = "soma-images-bucket";
        public static readonly string BucketURL = $"https://{BucketName}.s3.amazonaws.com";

        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.EUCentral1;
        private static IAmazonS3 s3Client;

        public static SomaEbookImage UploadImage(SomaEbookImage ebookImage)
        {
            string filepath_to_uploaded_image = ebookImage.ImageFilePath;
            string image_unique_key_name = ebookImage.ImageID;

            s3Client = new AmazonS3Client(bucketRegion);
            Status upload_result = UploadFileAsync(filepath_to_uploaded_image, image_unique_key_name).Result;

            ebookImage.StatusCode = upload_result.StatusCode;
            ebookImage.StatusDesc = upload_result.StatusDesc;
            ebookImage.WebFilePath = $"{BucketURL}/{image_unique_key_name}";

            return ebookImage;
        }

        public static List<SomaEbookImage> UploadImages(List<SomaEbookImage> ebook_images)
        {
            List<Task> upload_tasks = new List<Task>();

            foreach (var image in ebook_images)
            {
                string filepath_to_uploaded_image = image.ImageFilePath;
                string image_unique_key_name = image.ImageID;

                s3Client = new AmazonS3Client(bucketRegion);

                Task upload_task = UploadFileAsync(filepath_to_uploaded_image, image_unique_key_name)
                .ContinueWith((finished_upload) =>
                {
                    Status upload_result = finished_upload.Result;
                    image.StatusCode = upload_result.StatusCode;
                    image.StatusDesc = upload_result.StatusDesc;
                    image.WebFilePath = $"{BucketURL}/{image_unique_key_name}";
                });

                upload_tasks.Add(upload_task);
            }

            Task.WaitAll(upload_tasks.ToArray());

            return ebook_images;

        }

        private static void continuationAction(Task<Status> Result)
        {


        }

        private static async Task<Status> UploadFileAsync(string filepath_to_uploaded_image, string image_unique_key_name)
        {
            Status result = new Status();
            try
            {
                var fileTransferUtility = new TransferUtility(s3Client);


                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = BucketName,
                    FilePath = filepath_to_uploaded_image,
                    Key = image_unique_key_name,
                    CannedACL = S3CannedACL.PublicRead
                };

                await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
                return result;
            }
            catch (AmazonS3Exception e)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"Error. Message: '{e.Message}' when uploading an object";
                return result;
            }
            catch (Exception e)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"Error. Message: '{e.Message}' when uploading an object";
                return result;
            }
        }
    }
}
