﻿using HtmlAgilityPack;
using SomaEbookManager.EntityClasses;
using SomaEbookManager.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SomaEbookManager
{
    public abstract class EbookReader
    {
        protected static string ParseChapterTitle(string chapter_text, string prefferedTitle = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(prefferedTitle) && prefferedTitle.ToUpper().Contains("CHAPTER"))
                {
                    return prefferedTitle;
                }

                FastHtmlTextExtractor htmlTextExtractor = new FastHtmlTextExtractor();
                string plain_text = htmlTextExtractor.ExtractPlainText(chapter_text).Trim();
                string reduced_text = SomaSharedCommons.FirstWords(plain_text, 8);
                return reduced_text;
            }
            catch (Exception)
            {
                return "N/A";
            }
        }

        public static Status ChapterContainsText(string chapter_html)
        {
            Status result = new Status();
            try
            {
                string bodyText = GetTextFromHtml(chapter_html);

                if (!string.IsNullOrEmpty(bodyText))
                {
                    result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    result.StatusDesc = Globals.SUCCESS_TEXT;
                    return result;
                }

                bool sanitizedHtml = chapter_html.Contains("<img");

                if (sanitizedHtml)
                {
                    result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    result.StatusDesc = Globals.SUCCESS_TEXT;
                    return result;
                }

                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = "NO_TEXT";
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return result;


        }

        private static string GetTextFromHtml(string html)
        {
            string result = "";
            try
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);

                string bodyText = doc.DocumentNode?.SelectNodes("//text()").FirstOrDefault()?.InnerText;

                return bodyText;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public static Status ReadChapterContentFromPath(string chapter_file_path, List<SomaEbookImage> ebookImages)
        {
            Status result = new Status();
            try
            {

                HtmlDocument doc = new HtmlDocument();
                doc.Load(chapter_file_path, Encoding.UTF8);

                string bodyText = doc.DocumentNode.SelectSingleNode("//body").InnerHtml;
                string sanitizedHtml = bodyText.Replace("<image", "<img").Replace("xlink:href", "src").Replace("</image>", "</img>");
                result.ResultID = sanitizedHtml;


                if (ChapterContainsText(sanitizedHtml).StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"CHAPTER HAS NO TEXT";
                    return result;
                }

                return ReplaceImagePathsToWebPaths(sanitizedHtml, ebookImages);
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return result;
        }

        public static Status ReadChapterContent(string chapter_html, List<SomaEbookImage> ebookImages)
        {
            Status result = new Status();
            try
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(chapter_html);
                string sanitizedHtml = chapter_html.Replace("<image", "<img").Replace("xlink:href", "src").Replace("</image>", "</img>");
                result.ResultID = sanitizedHtml;

                if (ChapterContainsText(sanitizedHtml).StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"CHAPTER HAS NO TEXT";
                    return result;
                }

                return ReplaceImagePathsToWebPaths(sanitizedHtml, ebookImages);
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return result;
        }

        public static Status ReplaceImagePathsToWebPaths(string chapterContent, List<SomaEbookImage> ebookImages)
        {
            Status result = new Status();
            try
            {
                if (string.IsNullOrEmpty(chapterContent))
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"NO CHAPTER CONTENT FOUND. LOAD CONTENT INTO THE CHAPTER CONTENT FIELD";
                    return result;
                }

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(chapterContent);
                var nodes = doc.DocumentNode.SelectNodes("//img[@src]");

                //no img tags found in the html
                if (nodes == null)
                {
                    return CleanedHtml(chapterContent);
                }


                foreach (HtmlNode node in nodes)
                {
                    var imgSource_atrribute = node.Attributes["src"].Value;
                    imgSource_atrribute = imgSource_atrribute.Replace("/", Globals.DIR_SEPARATOR);
                    imgSource_atrribute = imgSource_atrribute.Split(new string[] { Globals.DIR_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

                    //find image for ebook chapter
                    SomaEbookImage img = ebookImages.Where(i => ((i.ImageFilePath.ToUpper().Contains(imgSource_atrribute?.ToUpper())))).FirstOrDefault();

                    if (img != null)
                    {
                        //change the src attribute of the image
                        node.SetAttributeValue("src", $"{img.WebFilePath}");
                        continue;
                    }

                    //find image for ebook chapter
                    img = ebookImages.Where(i => ((imgSource_atrribute.Contains(i.ImageID)))).FirstOrDefault();

                    if (img != null)
                    {
                        //change the src attribute of the image
                        node.SetAttributeValue("src", $"{img.WebFilePath}");
                        continue;
                    }

                }

                var content = doc.DocumentNode.OuterHtml;
                return CleanedHtml(content);
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return result;
        }

        private static Status CleanedHtml(string content)
        {
            Status result = new Status();
            content = SomaSharedCommons.RemoveUnwantedHtmlTagsButKeepChildTags(content, new List<string>() { "svg", "a" });
            content = ReplaceParagraphTagsWithBreaks(content);

            //remove link ( a href) tags
            content = ReplaceHtmlElement(content, "a", string.Empty);
            result.ResultID = content;
            result.StatusCode = Globals.SUCCESS_STATUS_CODE;
            result.StatusDesc = Globals.SUCCESS_TEXT;
            return result;
        }

        protected static string ParseTag(string html, string tag)
        {
            string returnString = string.Empty;
            string regex = string.Format("<{0}.*?>(?<info>.*?)</{0}", tag);
            Match result = Regex.Match(html, regex, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (result.Success)
                returnString = result.Groups["info"].Value;
            return string.IsNullOrEmpty(returnString) ? ParseMeta(html, tag) : returnString;
        }

        protected static string ParseMeta(string html, string tag)
        {
            string returnString = string.Empty;
            string regex = string.Format("<META name-\"{0}\" content=\"(?<content>.*?)\">", tag);
            Match result = Regex.Match(html, regex, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (result.Success)
                returnString = result.Groups["content"].Value;
            return returnString;
        }

        protected static string ReplaceHtmlElement(string html, string oldTag, string newTag)
        {
            Status result = new Status();
            try
            {

                if (String.IsNullOrEmpty(html))
                {
                    return html;
                }

                if (!html.Contains("<p"))
                {
                    return html;
                }
                string replacement_regex = $"<[{oldTag.ToLower()}{oldTag.ToUpper()}]+(>|.*?[^?]>)";
                html = Regex.Replace(html, replacement_regex, newTag);
                html = html.Replace($"</{oldTag}>", newTag);
                return html;
            }
            catch (Exception)
            {

            }

            return html;
        }

        protected static string ReplaceParagraphTagsWithBreaks(string html)
        {
            Status result = new Status();
            try
            {

                if (String.IsNullOrEmpty(html))
                {
                    return html;
                }

                if (!html.Contains("<p"))
                {
                    return html;
                }

                html= Regex.Replace(html, "<[pP]+(>|.*?[^?]>)", string.Empty);
                html = html.Replace("</p>", "</br></br>");
                return html;
            }
            catch (Exception)
            {

            }

            return html;
        }
    }
}
