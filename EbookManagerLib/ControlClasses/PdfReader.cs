﻿using Crabwise.PDFtoEPUB.Commands;
using HtmlAgilityPack;
using SomaEbookManager.EntityClasses;
using SomaEbookManager.HelperClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SomaEbookManager
{
    public class PDF_Reader : EbookReader
    {
        public static SomaEbook ExtractEbook(string pdf_file_path)
        {
            SomaEbook ebook = new SomaEbook();
            string output_folder_for_extracted_files = GetOutputFolder(pdf_file_path);

            try
            {
                if (string.IsNullOrEmpty(pdf_file_path))
                {
                    throw new Exception("No Path Supplied");
                }

                if (!File.Exists(pdf_file_path))
                {
                    throw new Exception($"Path not found: [{pdf_file_path}]");
                }

                if (!pdf_file_path.ToUpper().EndsWith(".PDF"))
                {
                    ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                    ebook.StatusDesc = "INVALID FILE FORMAT. PLEASE SUPPLY PATH TO VALID PDF FILE";
                    return ebook;
                }

                string html_content_from_pdf = GenerateHTMLFromFile(pdf_file_path);
                html_content_from_pdf = html_content_from_pdf.Replace(Environment.NewLine, string.Empty);

                //check for success
                if (string.IsNullOrEmpty(html_content_from_pdf))
                {
                    ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                    ebook.StatusDesc = "UNABLE TO READ PDF FILE. INTERNAL ERROR";
                    return ebook;
                }
                //load images first before attempting to read chapters
                //this is because chapter content may use/refer to those images
                ebook.EbookId = SharedCommons.GenerateUniqueId("EBOOK-");
                ebook.FilePath = pdf_file_path;
                ebook.EbookType = SomaEbookType.PDF;
                
                ebook.EbookImages = ReadImagesFromOutpitFolder(output_folder_for_extracted_files, ebook.EbookId);
                ebook.EbookChapters = ParseChaptersFromHtml(html_content_from_pdf,ebook.EbookId,ebook.EbookImages);
                ebook.Title = ParseTag(html_content_from_pdf, "Title");
                ebook.Author = GetEbookAurthor(html_content_from_pdf);
                ebook.Publisher = GetEbookPublisher(html_content_from_pdf);
                ebook.AuthorID = ebook.Author.AuthorID;
                ebook.PublisherID = ebook.Publisher.PublisherID;
                ebook.CoverImageID = GetCoverImage(ebook);

                //success
                ebook.StatusCode = Globals.SUCCESS_STATUS_CODE;
                ebook.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                ebook.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            finally
            {
                //SomaSharedCommons.DeleteOutputFolder(output_folder_for_extracted_files);
            }

            return ebook;
        }

        private static string GetCoverImage(SomaEbook ebook)
        {
            string coverImageID = SomaSharedCommons.ReadChaptersForFirstImage(ebook);
            return coverImageID;
        }

        private static string GetOutputFolder(string pdf_file_path)
        {
            string folder_path = "";
            try
            {
                return new FileInfo(pdf_file_path).DirectoryName;
            }
            catch
            {

            }
            return folder_path;
        }

        public static string GenerateHTMLFromFile(string pdfFilePath)
        {
            PdfToHtmlCommand cmd = new PdfToHtmlCommand
            {
                PDFFileLocation = pdfFilePath,
                WriteToStdOut = true,
                ExchangePDFLinksByHTML = true
            };

            Crabwise.CommandWrap.CommandStartInfo startinfo = new Crabwise.CommandWrap.CommandStartInfo
            {
                WorkingDirectory = Directory.GetCurrentDirectory(),
                Path = GetApplicationRoot(),//@"C:\Users\BROND\Downloads\pdftohtml-0.38-win32\";
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            };

            cmd.Execute(startinfo);
            return cmd.StandardOutput;
        }

        public static string GetApplicationRoot()
        {
            string assembly_location = GetAssembly().CodeBase;
            string exePath = new Uri(assembly_location).LocalPath;

            string directory_name = new FileInfo(exePath).DirectoryName;
            string pdf_to_html_filename = $"{directory_name}\\pdftohtml.exe";

            //pdf to html exe not found
            if (!File.Exists(pdf_to_html_filename))
            {
                throw new System.Exception("Unable to Find File: " + pdf_to_html_filename);
            }

            return directory_name;

        }

        private static Assembly GetAssembly()
        {
            if (System.Web.HttpContext.Current == null ||
            System.Web.HttpContext.Current.ApplicationInstance == null)
            {
                return Assembly.GetEntryAssembly();
            }

            var type = System.Web.HttpContext.Current.ApplicationInstance.GetType();
            while (type != null && type.Namespace == "ASP")
            {
                type = type.BaseType;
            }

            return type == null ? null : type.Assembly;
        }

        private static List<SomaEbookImage> ReadImagesFromOutpitFolder(string outPutFolderPath, string EbookId)
        {
            List<SomaEbookImage> all = new List<SomaEbookImage>();

            String searchFolder = outPutFolderPath;
            var filters = new String[] { "*.jpg", "*.jpeg", "*.JPG", "*.JPEG", "*.png", "*.jpe", "*.svg", "*.gif", "*.bmp" };
            var files = SomaSharedCommons.GetFilesMatchingPatternFromDirectory(searchFolder, filters, true);

            foreach (var filepath in files)
            {
                string img_name = Path.GetFileName(filepath);
                string img_file_path = filepath;
                SomaEbookImage img = new SomaEbookImage(img_name, img_file_path, EbookId);
                all.Add(img);
            }

            return all;
        }

        private static SomaEbookPublisher GetEbookPublisher(string html)
        {
            SomaEbookPublisher publisher = new SomaEbookPublisher();
            try
            {
                string publisherName = ParseMeta(html, "publisher");
                publisher.PublisherID = SharedCommons.GenerateUniqueId("PUBLISHER-");
                publisher.StatusCode = Globals.SUCCESS_STATUS_CODE;
                publisher.StatusDesc = Globals.SUCCESS_TEXT;
                publisher.PublisherName = string.IsNullOrEmpty(publisherName) ? Globals.UNKNOWN : publisherName; 
            }
            catch (Exception ex)
            {
                publisher.StatusCode = Globals.FAILURE_STATUS_CODE;
                publisher.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return publisher;
        }

        private static SomaEbookAuthor GetEbookAurthor(string html)
        {
            SomaEbookAuthor author = new SomaEbookAuthor();
            try
            {
                string authorName = ParseMeta(html, "author");
                author.AuthorID = SharedCommons.GenerateUniqueId("AUTHOR-");
                author.StatusCode = Globals.SUCCESS_STATUS_CODE;
                author.StatusDesc = Globals.SUCCESS_TEXT;
                author.AuthorName = string.IsNullOrEmpty(authorName) ? Globals.UNKNOWN : authorName; ;
            }
            catch (Exception ex)
            {
                author.StatusCode = Globals.FAILURE_STATUS_CODE;
                author.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return author;
        }

        private static List<SomaEbookChapter> ParseChaptersFromHtml(string html_content_from_pdf,string EbookId,List<SomaEbookImage> ebookImages)
        {
            List<SomaEbookChapter> all_chapters = new List<SomaEbookChapter>();

            HtmlDocument htmldoc = new HtmlDocument();
            htmldoc.LoadHtml(html_content_from_pdf);

            string body_text = htmldoc.DocumentNode.SelectSingleNode("//body").InnerHtml;

            //since conversion from pdf to html doesnt produce exact results
            //retrieving of chapters is not a trivial matter
            //I ended using the hr tag used to demaket the end of a page
            string[] chapter_separaters = new string[] { "<hr>" };

            //titles where in the first line of the chapter in the form below
            //<a name=23>The Tale of Tinúviel</a><br>
            string[] body_text_parts = body_text.Split(chapter_separaters, StringSplitOptions.None);
            int internal_chapter_index = 1;
            int external_chapter_index = 1;

            foreach (var chapter_text in body_text_parts)
            {
                string chapter_content = chapter_text.Replace(Environment.NewLine, string.Empty);

                //something is wrong with the chapter content
                if (string.IsNullOrEmpty(chapter_content))
                {
                    //throw new Exception($"UNABLE TO PARSE CHAPTER [{internal_chapter_index}] CONTENT.");
                    continue;
                }
                Status read_status = ReadChapterContent(chapter_content, ebookImages);

                //something is wrong with the chapter content
                if (read_status.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    //throw new Exception($"UNABLE TO PARSE CHAPTER [{internal_chapter_index}] CONTENT.");
                    continue;
                }

                SomaEbookChapter chapter = new SomaEbookChapter();

                chapter.ChapterId = SharedCommons.GenerateUniqueId("CHAPTER-");
                chapter.IdRef = "N/A";
                chapter.EbookId = EbookId;
                chapter.ChapterCoverImageId = "NOT_FOUND";
                chapter.ChapterTitle = ParseChapterTitle(chapter_content);
                chapter.ChapterContent = read_status.ResultID;
                chapter.ChapterIndex_Internal = internal_chapter_index;
                chapter.IsTrueChapter = CheckIfChapterIsATrueChapterStart(chapter);
                chapter.ChapterIndex_External = chapter.IsTrueChapter ? ++external_chapter_index : 0;

                if (chapter.IsValid())
                    all_chapters.Add(chapter);


                internal_chapter_index++;
            }

            List<SomaEbookChapter> true_chapters = all_chapters.Where(i => i.IsTrueChapter).ToList();

            foreach (var true_chapter in true_chapters)
                Console.WriteLine($"Chapter Title: {true_chapter.ChapterTitle}");

            return all_chapters;
        }

        private static string ParseChapterTitle(string chapter_text)
        {
            try
            {

                FastHtmlTextExtractor htmlTextExtractor = new FastHtmlTextExtractor();
                string plain_text = htmlTextExtractor.ExtractPlainText(chapter_text).Trim();
                string reduced_text = SomaSharedCommons.FirstWords(plain_text, 8);
                if (!string.IsNullOrEmpty(reduced_text))
                    return reduced_text;

                HtmlDocument htmldoc = new HtmlDocument();
                string[] title_separaters = new string[] { "<br>", "<br/>" };
                string dirty_chapter_title = chapter_text.Split(title_separaters, StringSplitOptions.None).FirstOrDefault()?.Replace(Environment.NewLine, string.Empty);
                string dirty_html_wrap = $"{dirty_chapter_title}";
                htmldoc.LoadHtml(dirty_chapter_title);
                string clean_chapter_title = htmldoc.DocumentNode?.SelectSingleNode("//text()")?.InnerText?.Trim();
                clean_chapter_title = SomaSharedCommons.RemoveSpecialCharacters(clean_chapter_title);

                //title found
                if (!string.IsNullOrEmpty(clean_chapter_title))
                    return clean_chapter_title;

                //no title found
                dirty_chapter_title = SomaSharedCommons.FirstWords(chapter_text, 10);
                htmldoc.LoadHtml(dirty_chapter_title);
                clean_chapter_title = htmldoc.DocumentNode?.SelectSingleNode("//text()")?.InnerText?.Trim();
                clean_chapter_title = SomaSharedCommons.RemoveSpecialCharacters(clean_chapter_title);
                return clean_chapter_title;
            }
            catch (Exception)
            {
                return "N/A";
            }
        }

        private static bool CheckIfChapterIsATrueChapterStart(SomaEbookChapter chapter)
        {
            if (string.IsNullOrEmpty(chapter.ChapterTitle?.Trim()))
                return false;
            if (chapter.ChapterTitle == chapter.ChapterTitle.ToUpper())
                return true;
            if (chapter.ChapterTitle.Trim().ToUpper().StartsWith("CHAPTER"))
                return true;
            if (chapter.ChapterTitle.Trim().ToUpper().StartsWith("C H A P T E R"))
                return true;
            return false;
        }

     
    }
}
