﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SomaEbookManager.EntityClasses;

namespace SomaEbookManager
{
    public class BussinessLogic
    {
        public List<SomaEbook> GetAllUsersEbooks(string userID)
        {
            List<SomaEbook> all = SomaEbook.QueryWithStoredProc("GetEbooksByUserId", userID).ToList();
            all.ForEach(i => { if (string.IsNullOrEmpty(i.CoverImageID)) { i.CoverImageID = "NOT_FOUND"; } });
            //all.ForEach(i => { if (string.IsNullOrEmpty(i.CoverImageID)) { i.CoverImageID = "NOT_FOUND"; } });
            return all;
        }

        public Status DeleteEbook(string ownerID, string password, string EbookId)
        {
            Status status = new Status();
            try
            {
                SomaSystemUser systemUser = VerifyUserCredentials(ownerID, password);

                if (systemUser.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    status.StatusCode = Globals.FAILURE_STATUS_CODE;
                    status.StatusDesc = systemUser.StatusDesc;
                    return status;
                }

                SomaEbook ebook = GetEbookByIDAndOwnerID(ownerID, EbookId);

                if (ebook.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    status.StatusCode = Globals.FAILURE_STATUS_CODE;
                    status.StatusDesc = ebook.StatusDesc;
                    return status;
                }

                int rows_affected = ebook.DeleteWithStoredProcAutoParams("DeleteEbook");

                if (rows_affected <= 0)
                {
                    status.StatusCode = Globals.FAILURE_STATUS_CODE;
                    status.StatusDesc = $"No Rows Affected. [{rows_affected}]";
                    return status;
                }


                status.StatusCode = Globals.SUCCESS_STATUS_CODE;
                status.StatusDesc = $"Ebook [{ebook.Title}] Has been permanently removed from your Libary";
                return status;

            }
            catch (Exception ex)
            {
                status.StatusCode = Globals.FAILURE_STATUS_CODE;
                status.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return status;
        }

        public SomaEbook GetEbookByIDAndOwnerID(string userID, string EbookId)
        {
            SomaEbook ebook = new SomaEbook();
            try
            {
                ebook = SomaEbook.QueryWithStoredProc("GetEbookById", EbookId).FirstOrDefault();

                if (ebook != null)
                {

                    return ebook;
                }

                ebook = new SomaEbook
                {
                    StatusCode = Globals.FAILURE_STATUS_CODE,
                    StatusDesc = $"EBOOK WITH ID [{EbookId}] NOT FOUND UNDER PROFILE FOR USER [{userID}]"
                };

                return ebook;

            }
            catch (Exception ex)
            {
                ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                ebook.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return ebook;
        }

        public Status LendEbookToOtherUser(LendRequest lendRequest)
        {
            Status result = new Status();
            try
            {

                if (!lendRequest.IsValidLendOutRequest())
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = lendRequest.StatusDesc;
                    return result;
                }

                //find ebook
                SomaEbook aBook = SomaEbook.QueryWithStoredProc("GetEbookByIdAndOwnerId", lendRequest.CurrentOwnerID, lendRequest.EbookID).FirstOrDefault();

                //if its not found
                if (aBook == null)
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"EBOOK WITH ID [{lendRequest.EbookID}] NOT FOUND UNDER USER ACCOUNT [{lendRequest.CurrentOwnerID}]";
                    return result;
                }

                //find the new reader
                SomaSystemUser theNewReader = SomaSystemUser.QueryWithStoredProc("GetSystemUserById", lendRequest.NewReaderID).FirstOrDefault();

                //we cant find the new reader
                if (theNewReader == null)
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"NEW READER WITH ID [{lendRequest.NewReaderID}] NOT FOUND";
                    return result;
                }

                //lend the ebook out
                int.TryParse(lendRequest.DurationInHours, out int duration_in_hours);

                aBook.CurrentReaderID = lendRequest.NewReaderID;
                aBook.IsLentOut = true;
                aBook.LentOutOn = DateTime.Now;
                aBook.HoursLentOut = duration_in_hours;

                //save changes
                aBook.SaveWithStoredProcAutoParams("LendEbook");


                //log action
                SaveIntoAuditTrail(lendRequest.CurrentOwnerID, AuditLogType.LEND_ACTION, $"{lendRequest.CurrentOwnerID} LENT EBOOK [{lendRequest.EbookID}] TO {lendRequest.NewReaderID}");

                //success
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = $"Ebook [{aBook.Title}] has been lent to [{aBook.CurrentReaderID}] for [{lendRequest.DurationInHours}] hours";
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return result;
        }

        public SomaEbookChapter GetEbookChapterByChapterIndex_External(string userID, string ebookId, string chapterId)
        {
            SomaEbookChapter chapter = new SomaEbookChapter();
            try
            {
                if (string.IsNullOrEmpty(ebookId))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply an Ebook ID";
                    return chapter;
                }

                if (string.IsNullOrEmpty(chapterId))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply a Chapter ID";
                    return chapter;
                }

                if (string.IsNullOrEmpty(userID))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply a User ID";
                    return chapter;
                }

                chapter = SomaEbookChapter.QueryWithStoredProc("GetEbookChapterByExternalChapterId", ebookId, chapterId, userID).FirstOrDefault();


                if (chapter != null)
                {
                    chapter.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    chapter.StatusDesc = Globals.SUCCESS_TEXT;
                    return chapter;
                }

                chapter = CheckIfEbookHasBeenLentOut(userID, ebookId, chapterId);

                if (chapter != null)
                {

                    return chapter;
                }

                chapter = new SomaEbookChapter
                {
                    StatusCode = Globals.FAILURE_STATUS_CODE,
                    StatusDesc = $"CHAPTER WITH ID [{chapterId}] NOT FOUND"
                };

                return chapter;
            }
            catch (Exception ex)
            {
                chapter = new SomaEbookChapter();
                chapter.StatusCode = Globals.FAILURE_STATUS_CODE;
                chapter.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return chapter;
        }

        private SomaEbookChapter CheckIfEbookHasBeenLentOut(string userID, string ebookId, string chapterId)
        {
            SomaEbookChapter ebookChapter = new SomaEbookChapter();
            try
            {
                SomaEbook ebook = SomaEbook.QueryWithStoredProc("CheckIfEbookHasBeenLentOut", userID, ebookId,DateTime.Now).FirstOrDefault();

                if (ebook == null)
                {
                    ebookChapter.StatusCode = Globals.FAILURE_STATUS_CODE;
                    ebookChapter.StatusDesc = $"CHAPTER WITH ID [{chapterId}] NOT FOUND IN BOOK [{ebookId}]";
                    return ebookChapter;
                }

                if (ebook.IsLentOut)
                {
                    ebookChapter.StatusCode = Globals.FAILURE_STATUS_CODE;
                    ebookChapter.StatusDesc = $"EBOOK [{ebook.Title}] WAS LENT TO [{ebook.CurrentReaderID}] FOR [{ebook.HoursLentOut}] HOUR(S) UNTIL [{ebook.LentOutOn.AddHours(ebook.HoursLentOut).ToString("yyyy-MM-dd HH:mm:ss")}]";
                    return ebookChapter;
                }


                return null;
            }
            catch (Exception ex)
            {
                ebookChapter.StatusCode = Globals.FAILURE_STATUS_CODE;
                ebookChapter.StatusDesc = $"ERROR: {ex.Message}";
                return ebookChapter;
            }
        }

        internal SomaSystemUser GetUserByToken(string token)
        {
            SomaSystemUser user = new SomaSystemUser();
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    user.StatusCode = Globals.FAILURE_STATUS_CODE;
                    user.StatusDesc = $"ERROR: INVALID TOKEN SUPPLIED";
                    return user;
                }

                SessionToken sessionToken = SessionToken.QueryWithStoredProc("GetTokenById", token).FirstOrDefault();

                if (sessionToken == null)
                {
                    user.StatusCode = Globals.FAILURE_STATUS_CODE;
                    user.StatusDesc = $"ERROR: INVALID TOKEN SUPPLIED";
                    return user;
                }

                SomaSystemUser systemUser = SomaSystemUser.QueryWithStoredProc("GetSystemUserById", sessionToken.UserId).FirstOrDefault();

                if (systemUser == null)
                {
                    user.StatusCode = Globals.FAILURE_STATUS_CODE;
                    user.StatusDesc = $"ERROR: UNABLE TO DETERMINE USER ATTACHED TO TOKEN";
                    return user;
                }

                user = systemUser;
                user.StatusCode = Globals.SUCCESS_STATUS_CODE;
                user.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                user.StatusCode = Globals.FAILURE_STATUS_CODE;
                user.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return user;
        }

        internal SomaSystemUser GetUserByUsername(string username)
        {
            SomaSystemUser user = new SomaSystemUser();
            try
            {
                SomaSystemUser systemUser = SomaSystemUser.QueryWithStoredProc("GetSystemUserById", username).FirstOrDefault();

                if (systemUser == null)
                {
                    user.StatusCode = Globals.FAILURE_STATUS_CODE;
                    user.StatusDesc = $"ERROR: NO USER WITH USERNAME [{username}] FOUND";
                    return user;
                }

                user = systemUser;
                user.StatusCode = Globals.SUCCESS_STATUS_CODE;
                user.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                user.StatusCode = Globals.FAILURE_STATUS_CODE;
                user.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return user;
        }


        internal SomaEbookImage GetEbookImage(string imageID)
        {
            SomaEbookImage image = new SomaEbookImage();
            try
            {
                SomaEbookImage dbImage = SomaEbookImage.QueryWithStoredProc("GetImageById", imageID).FirstOrDefault();

                if (dbImage == null)
                {
                    //if the seeding of the DB went well then
                    //a not found default image is automatically inserted
                    return GetEbookImage("NOT_FOUND");
                }

                image = dbImage;
                image.StatusCode = Globals.SUCCESS_STATUS_CODE;
                image.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                image.StatusCode = Globals.FAILURE_STATUS_CODE;
                image.StatusDesc = $"ERROR: {ex.Message}";
            }
            return image;
        }



        public SomaEbookChapter GetEbookChapterByChapterIndex_Internal(string userID, string ebookId, string chapter_index)
        {
            SomaEbookChapter chapter = new SomaEbookChapter();
            try
            {
                if (string.IsNullOrEmpty(ebookId))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply an Ebook ID";
                    return chapter;
                }

                if (string.IsNullOrEmpty(chapter_index))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply a Chapter ID";
                    return chapter;
                }

                if (string.IsNullOrEmpty(userID))
                {
                    chapter.StatusCode = Globals.VALIDATION_FAILURE_STATUS_CODE;
                    chapter.StatusDesc = $"Please Supply a User ID";
                    return chapter;
                }

                chapter = SomaEbookChapter.QueryWithStoredProc("GetEbookChapterByInternalChapterId", ebookId, chapter_index, userID).FirstOrDefault();

                if (chapter != null)
                {
                    chapter.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    chapter.StatusDesc = Globals.SUCCESS_TEXT;
                    return chapter;
                }

                chapter = CheckIfEbookHasBeenLentOut(userID, ebookId, chapter_index);

                if (chapter != null)
                {
                    return chapter;
                }

                chapter = new SomaEbookChapter
                {
                    StatusCode = Globals.FAILURE_STATUS_CODE,
                    StatusDesc = $"CHAPTER WITH ID [{chapter_index}] NOT FOUND"
                };
                return chapter;

            }
            catch (Exception ex)
            {
                chapter = new SomaEbookChapter();
                chapter.StatusCode = Globals.FAILURE_STATUS_CODE;
                chapter.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return chapter;
        }

        public Status BookMarkEbookChapter(string userID, string ebookId, string chapter_index, string chapterScrollbarPosition = null)
        {
            Status result = new Status();
            try
            {
                //chapter has been found
                //bookmark it in Ebook
                SomaEbook ebook = GetEbookByIDAndOwnerID(userID, ebookId);

                //error retrieving Ebook
                if (ebook.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    result.StatusDesc = Globals.SUCCESS_TEXT;
                    return result;
                }

                //ebook found
                Status bookmark_result = BookMarkEbookChapter(chapter_index, ebook);

                if (chapterScrollbarPosition == null)
                {
                    result.StatusCode = bookmark_result.StatusCode;
                    result.StatusDesc = bookmark_result.StatusCode;
                    return result;
                }

                SomaEbookChapter ebookChapter = GetEbookChapterByChapterIndex_Internal(userID, ebookId, chapter_index);
                ebookChapter.UpdateWithStoredProcAsync("UpdateChapterScrollPosition", ebookId, chapter_index, chapterScrollbarPosition);

                result.StatusCode = bookmark_result.StatusCode;
                result.StatusDesc = bookmark_result.StatusCode;
                return result;

            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return result;
        }

        public SomaEbook ConvertEbookFileFormat(string ebookID, SomaEbookType toFileFormat)
        {
            SomaEbook somaEbook = new SomaEbook();
            try
            {
                somaEbook.StatusCode = Globals.SUCCESS_STATUS_CODE;
                somaEbook.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                somaEbook.StatusCode = Globals.FAILURE_STATUS_CODE;
                somaEbook.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return somaEbook;
        }

        public SomaSystemUser VerifyUserCredentials(string userId, string Password)
        {
            SomaSystemUser db_user = new SomaSystemUser();
            try
            {
                db_user = SomaSystemUser.QueryWithStoredProc("GetSystemUserById", userId).FirstOrDefault();

                //error finding user
                if (db_user.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    db_user.StatusCode = Globals.FAILURE_STATUS_CODE;
                    db_user.StatusDesc = $"USER WITH ID {userId} NOT FOUND";
                    return db_user;
                }

                //hash entered password
                string hashedPassword = SomaSharedCommons.HashPassword(Password);

                //if the hashes dont match
                if (hashedPassword != db_user.Password && Password != db_user.Password)
                {
                    db_user = new SomaSystemUser()
                    {
                        StatusCode = Globals.FAILURE_STATUS_CODE,
                        StatusDesc = $"INVALID PASSWORD SUPPLIED"
                    };
                    return db_user;
                }

                //hashes match..user is vald
                db_user.StatusCode = Globals.SUCCESS_STATUS_CODE;
                db_user.StatusDesc = "SUCCESS";

                //log action in audit trail
                SaveIntoAuditTrail(userId, AuditLogType.LOGIN, $"{db_user.Username} LOGGED IN");

            }
            catch (Exception ex)
            {
                db_user = new SomaSystemUser()
                {
                    StatusCode = Globals.FAILURE_STATUS_CODE,
                    StatusDesc = $"EXCEPTION: {ex.Message}"
                };
            }
            return db_user;
        }

        public SessionToken Login(string userId, string Password)
        {
            SessionToken token = new SessionToken();
            try
            {
                SomaSystemUser user = VerifyUserCredentials(userId, Password);

                //error finding user
                if (user.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    token.StatusCode = Globals.FAILURE_STATUS_CODE;
                    token.StatusDesc = user.StatusDesc; //$"INVALID USERNAME OR PASSWORD";
                    return token;
                }


                //generate new token
                token.UserId = user.Username;
                token.TokenValue = SharedCommons.GenerateUniqueId("TOK");
                token.CreatedAt = DateTime.Now;
                token.StatusCode = Globals.SUCCESS_STATUS_CODE;
                token.StatusDesc = "SUCCESS";
                token.SaveWithStoredProcAutoParams("SaveToken");

                //log action in audit trail
                SaveIntoAuditTrail(userId, AuditLogType.LOGIN, $"TOKEN GENERATED FOR: {token.UserId}");

            }
            catch (Exception ex)
            {
                token = new SessionToken()
                {
                    StatusCode = Globals.FAILURE_STATUS_CODE,
                    StatusDesc = $"EXCEPTION: {ex.Message}"
                };
            }
            return token;
        }

        public Status LogOutSystemUser(SomaSystemUser user)
        {
            Status result = new Status();
            SaveIntoAuditTrail(user.Username, AuditLogType.LOGOUT, $"{user.Username} Logged Out Successfully");
            result.StatusCode = Globals.SUCCESS_STATUS_CODE;
            result.StatusDesc = Globals.SUCCESS_TEXT;
            return result;
        }

        public Status SaveIntoAuditTrail(string userID, AuditLogType auditLogType, string Desc)
        {
            Status result = new Status();
            try
            {
                SomaAuditLog log = new SomaAuditLog()
                {
                    ActivityDateTime = DateTime.Now,
                    ActivityDescription = Desc,
                    ActivityTypeCode = auditLogType.ToString(),
                    UserId = userID
                };

                log.SaveWithStoredProcAutoParamsAsync("SaveAuditLog");
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return result;
        }

        public Status SaveSystemUser(SomaSystemUser user)
        {
            Status result = new Status();
            try
            {
                if (!user.IsValid())
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = user.StatusDesc;
                    return result;
                }

                //hash password
                user.Password = SomaSharedCommons.HashPassword(user.Password);

                //save user
                user.Save();
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return result;
        }

        public List<SomaEbook> ExtractAndSaveEbooks(string userID, params string[] ebooks_filepaths)
        {
            List<SomaEbook> upload_results = new List<SomaEbook>();
            try
            {

                Parallel.ForEach(ebooks_filepaths, filepath_to_ebook =>
               {
                   SomaEbook extracted_ebook = new SomaEbook();

                   SomaEbookType ebook_type = SomaSharedCommons.GetEbookType(filepath_to_ebook);

                   switch (ebook_type)
                   {
                       case SomaEbookType.EPUB:
                           extracted_ebook = EPUB_Reader.ExtractEbook(filepath_to_ebook);
                           break;
                       case SomaEbookType.PDF:
                           extracted_ebook = PDF_Reader.ExtractEbook(filepath_to_ebook);
                           break;
                       default:
                           extracted_ebook = new SomaEbook()
                           {
                               StatusCode = Globals.FAILURE_STATUS_CODE,
                               StatusDesc = $"EBOOK FILE FORMAT for [{GetFileName(filepath_to_ebook)}] IS NOT SUPPORTED. UPLOAD A PDF OR EPUB file"
                           };
                           break;
                   }

                   if (extracted_ebook.StatusCode != Globals.SUCCESS_STATUS_CODE)
                   {
                       upload_results.Add(extracted_ebook);
                       return;//continue;
                   }

                   if (!extracted_ebook.IsValid())
                   {
                       upload_results.Add(extracted_ebook);
                       return;//continue;
                   }

                   //save everything to the database
                   extracted_ebook.OwnerID = userID;
                   extracted_ebook.CurrentReaderID = userID;
                   extracted_ebook.FilePath = filepath_to_ebook;
                   extracted_ebook.EbookTypeID = extracted_ebook.EbookType.ToString();
                   extracted_ebook.CurrentChapterIndex = 1;
                   extracted_ebook.EbookHash = SomaSharedCommons.GenerateHashForFile(filepath_to_ebook);

                   int rows_affected = extracted_ebook.SaveWithStoredProcAutoParams("SaveEbook");

                   //if ebook hash is unique to owner
                   //and ebook has been saved successfully
                   if (rows_affected > 0)
                   {
                       extracted_ebook.Author.SaveWithStoredProcAutoParamsAsync("SaveAuthor");
                       extracted_ebook.Publisher.SaveWithStoredProcAutoParamsAsync("SavePublisher");
                       extracted_ebook.EbookImages.ForEach(i => i.SaveWithStoredProcAutoParamsAsync("SaveImage"));
                       extracted_ebook.EbookChapters.ForEach(i => i.SaveWithStoredProcAutoParams("SaveChapter"));
                   }
                   //Task.WaitAll();
                   upload_results.Add(extracted_ebook);
               });

                //result = GetBatchUploadResult(successfull_ebook_uploads, failed_ebook_uploads);
            }
            catch (Exception ex)
            {
                //result.StatusCode = Globals.FAILURE_STATUS_CODE;
                //result.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return upload_results;
        }

        private object GetFileName(string filepath_to_ebook)
        {
            try
            {
                return string.IsNullOrEmpty(filepath_to_ebook) ? "" : Path.GetFileName(filepath_to_ebook);
            }
            catch (Exception)
            {

                return filepath_to_ebook;
            }
        }

        private Status GetBatchUploadResult(List<SomaEbook> validEbooks, List<SomaEbook> failedEbooks)
        {
            Status result = new Status();
            if (failedEbooks.Any())
            {
                string msg = $"OF {validEbooks.Count + failedEbooks.Count} BOOKS, {validEbooks.Count} SUCCEEDED, {failedEbooks.Count} FAILED.<br/>";

                foreach (var failedBook in failedEbooks)
                {
                    msg += $"EBOOK [{failedBook.Title}]: {failedBook.StatusDesc} <br/>";
                }

                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = msg;
            }
            else
            {
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            return result;
        }

        public Status BookMarkEbookChapter(string chapter_index, SomaEbook ebook)
        {
            Status result = new Status();
            try
            {
                ebook.SaveWithStoredProcAsync("BookMarkChapter", ebook.EbookId, int.Parse(chapter_index));
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"ERROR:{ex.Message}";
            }
            return result;
        }

        public Status SaveAsHtml(string filepath, SomaEbook ebook)
        {
            Status result = new Status();
            try
            {
                //create the folders in the path if they dont exist
                Status create_path_result = SomaSharedCommons.CreateFilePathIfNotExists(filepath);

                //failed to create folders
                if (create_path_result.StatusCode != Globals.SUCCESS_STATUS_CODE)
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = create_path_result.StatusDesc;
                    return result;
                }

                //get root folder of file
                string folder_directory = SomaSharedCommons.GetDirectoryOfFilePath(filepath);

                //write out the images in the output folder
                ebook.EbookImages.ForEach(i =>
                {
                    i.WebFilePath = i.ImageName;
                    File.WriteAllBytes(folder_directory + i.ImageName, i.Image);
                });

                StringBuilder string_builder = new StringBuilder();

                //change the image paths in the ebook chapters to point
                //to images in the output folder
                foreach (var chapter in ebook.EbookChapters)
                {
                    string chapter_content = chapter.ChapterContent;
                    Status status = EPUB_Reader.ReplaceImagePathsToWebPaths(chapter_content, ebook.EbookImages);

                    if (status.StatusCode != Globals.SUCCESS_STATUS_CODE) continue;

                    chapter_content = status.ResultID;
                    string_builder.Append("<hr>");
                    string_builder.Append(chapter_content);
                }

                //write out the ebook itself
                string content_to_write = string_builder.ToString();
                File.WriteAllText(filepath, content_to_write);

                //success
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
                result.ResultID = filepath;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"ERROR:{ex.Message}";
            }
            return result;
        }
    }
}
