﻿using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using SomaEbookManager.EntityClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SomaEbookManager
{
    public class EPUB_Reader : EbookReader
    {
        //EPUBs are just ZIP files
        private static SomaEbook ExtractAndReadEbookContent(string filepath_to_ebook)
        {
            SomaEbook ebook = new SomaEbook();

            string output_folder_for_extracted_files = GenerateOutputFolder(filepath_to_ebook);

            try
            {
                bool success = SomaSharedCommons.ExtractZipFile(filepath_to_ebook, "", output_folder_for_extracted_files);

                if (!success)
                {
                    ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                    ebook.StatusDesc = "UNABLE TO EXTRACT EPUB CONTENTS. FILE MAYBE CORRUPTED";
                }

                ebook.EbookId = SharedCommons.GenerateUniqueId("EBOOK-");
                ebook.FilePath = filepath_to_ebook;
                ebook.EbookType = SomaEbookType.EPUB;

                //load images first before attempting to read chapters
                //this is because chapter content may use images
                ebook.EbookImages = ReadImagesFromOutputFolder(output_folder_for_extracted_files, ebook.EbookId);
                ebook.EbookChapters = ReadChaptersBasedOnManifest(output_folder_for_extracted_files, ebook.EbookId, ebook.EbookImages);
                ebook.CoverImageID = GetCoverImage(output_folder_for_extracted_files, ebook);
                ebook.Title = GetEbookTitle(output_folder_for_extracted_files);
                ebook.Author = GetEbookAurthor(output_folder_for_extracted_files);
                ebook.Publisher = GetEbookPublisher(output_folder_for_extracted_files);
                ebook.AuthorID = ebook.Author.AuthorID;
                ebook.PublisherID = ebook.Publisher.PublisherID;

                //success
                ebook.StatusCode = Globals.SUCCESS_STATUS_CODE;
                ebook.StatusDesc = Globals.SUCCESS_TEXT;

            }
            catch (Exception ex)
            {
                ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                ebook.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            finally
            {
                SomaSharedCommons.DeleteOutputFolder(output_folder_for_extracted_files);
            }

            return ebook;
        }

        private static string GenerateOutputFolder(string filepath_to_ebook)
        {
            string OutPutFolderPath = "N/A";
            try
            {
                string FileExt = Path.GetExtension(filepath_to_ebook);
                string FileName = Path.GetFileNameWithoutExtension(filepath_to_ebook);
                string FileDirectory = Path.GetDirectoryName(filepath_to_ebook);
                string TimeNow = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss_fff");
                OutPutFolderPath = FileDirectory + Globals.DIR_SEPARATOR + TimeNow + Globals.DIR_SEPARATOR;
                return OutPutFolderPath;
            }
            catch
            {
                return OutPutFolderPath;
            }
        }






        private static List<SomaEbookImage> ReadImagesFromOutputFolder(string outPutFolderPath, string EbookId)
        {
            List<SomaEbookImage> all = new List<SomaEbookImage>();

            String searchFolder = outPutFolderPath;
            var filters = new String[] { "*.jpg", "*.jpeg", "*.JPG", "*.JPEG", "*.png", "*.jpe", "*.svg", "*.gif", "*.bmp" };
            var files = SomaSharedCommons.GetFilesMatchingPatternFromDirectory(searchFolder, filters, true);

            foreach (var filepath in files)
            {
                string img_name = Path.GetFileName(filepath);
                string img_file_path = filepath;
                SomaEbookImage img = new SomaEbookImage(img_name, img_file_path, EbookId);
                all.Add(img);
            }
            return all;
        }


        private static List<SomaEbookChapter> OrderChaptersBasedOnSpine(string outPutFilePath, List<SomaEbookChapter> ebookChapters)
        {
            List<SomaEbookChapter> allChapters = new List<SomaEbookChapter>();

            try
            {
                //for epub..on extraction of zip file
                //there is a .opf  file that has a spine tag that describes order of the content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                if (filteredFiles.Length == 0)
                {
                    throw new Exception("NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FORMAT. EPUB MAYBE CORRUPTED");
                }

                string manifestFileName = filteredFiles[0];

                //initialize reader to read spine xml
                XmlReader reader = XmlReader.Create(manifestFileName);

                //go to spine tag
                bool success = reader.ReadToFollowing("spine");

                //if its not found
                //return now because we cant order the chapters
                if (!success)
                {
                    return ebookChapters;
                }

                int chapter_index = 1;

                while (reader.Read())
                {
                    if ((reader.NodeType != XmlNodeType.Element))
                    {
                        continue;
                    }

                    if (reader.Name.ToUpper() != "itemref".ToUpper())
                    {
                        continue;
                    }

                    if (!reader.HasAttributes)
                    {
                        continue;
                    }

                    //get id of chapter
                    string chapter_idRef = reader.GetAttribute("idref");

                    //find that chapter from all the chapters
                    SomaEbookChapter chapter = ebookChapters.Where(i => i.IdRef.ToUpper().Trim() == chapter_idRef.ToUpper().Trim()).FirstOrDefault();

                    //if not found
                    if (chapter == null)
                    {
                        continue;
                    }

                    //give thatchapter its right location in the ebook
                    chapter.ChapterIndex_External = chapter_index;
                    chapter.ChapterIndex_Internal = chapter_index;
                    allChapters.Add(chapter);
                    chapter_index++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UNABLE TO ORDER CHAPTERS IN EPUB: " + ex.Message);
            }

            return allChapters;
        }

        private static string GetEbookTitle(string outPutFilePath)
        {
            string Title = "";
            try
            {
                //for epub..on extraction of zip file
                //there is a file called content.opf that has a manifest that describes location of content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                if (filteredFiles.Length == 0)
                {
                    throw new Exception("NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FORMAT. EPUB MAYBE CORRUPTED");
                }

                string manifestFileName = filteredFiles[0];

                //initialize reader to read manifest xml
                XmlReader reader = XmlReader.Create(manifestFileName);

                //go to manifest tag
                bool success = reader.ReadToFollowing("dc:title");

                //if its not found
                if (!success)
                {
                    return Title;
                }

                //its found
                Title = reader.ReadElementContentAsString();

            }
            catch (Exception)
            {

            }
            return Title;
        }

        private static SomaEbookAuthor GetEbookAurthor(string outPutFilePath)
        {
            SomaEbookAuthor author = new SomaEbookAuthor();
            try
            {
                author.AuthorID = SharedCommons.GenerateUniqueId("AUTHOR-");
                //for epub..on extraction of zip file
                //there is a file called content.opf that has a manifest that describes location of content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                if (filteredFiles.Length == 0)
                {
                    author.StatusCode = Globals.FAILURE_STATUS_CODE;
                    author.StatusDesc = "NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FILE FORMAT.";
                    return author;
                }

                string manifestFileName = filteredFiles[0];

                //initialize reader to read manifest xml
                XmlReader reader = XmlReader.Create(manifestFileName);

                //go to manifest tag
                bool success = reader.ReadToFollowing("dc:creator");


                //if its not found
                if (success)
                {
                    string authorName = reader.ReadElementContentAsString();
                    author.AuthorID = SharedCommons.GenerateUniqueId("AUTHOR-");
                    author.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    author.StatusDesc = Globals.SUCCESS_TEXT;
                    author.AuthorName = authorName;
                    return author;
                }

                //go to manifest tag
                success = reader.ReadToFollowing("dc:author");


                //if its not found
                if (success)
                {
                    string authorName = reader.ReadElementContentAsString();
                    author.AuthorID = SharedCommons.GenerateUniqueId("AUTHOR-");
                    author.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    author.StatusDesc = Globals.SUCCESS_TEXT;
                    author.AuthorName = authorName;
                    return author;
                }

                author.StatusCode = Globals.SUCCESS_STATUS_CODE;
                author.StatusDesc = Globals.SUCCESS_TEXT;
                author.AuthorName = Globals.UNKNOWN;
                return author;
            }
            catch (Exception ex)
            {
                author.StatusCode = Globals.FAILURE_STATUS_CODE;
                author.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return author;
        }

        private static SomaEbookPublisher GetEbookPublisher(string outPutFilePath)
        {
            SomaEbookPublisher publisher = new SomaEbookPublisher();
            try
            {
                publisher.PublisherID = SharedCommons.GenerateUniqueId("PUBLISHER-");
                //for epub..on extraction of zip file
                //there is a file called content.opf that has a manifest that describes location of content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                if (filteredFiles.Length == 0)
                {
                    publisher.StatusCode = Globals.FAILURE_STATUS_CODE;
                    publisher.StatusDesc = ("NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FORMAT. EPUB MAYBE CORRUPTED");
                    return publisher;
                }

                string manifestFileName = filteredFiles[0];

                //initialize reader to read manifest xml
                XmlReader reader = XmlReader.Create(manifestFileName);

                //go to manifest tag
                bool success = reader.ReadToFollowing("dc:publisher");

                //if its not found
                if (!success)
                {
                    publisher.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    publisher.StatusDesc = Globals.SUCCESS_TEXT;
                    publisher.PublisherName = Globals.UNKNOWN;
                    return publisher;
                }

                //we return the publisher
                string publisherName = reader.ReadElementContentAsString();
                publisher.StatusCode = Globals.SUCCESS_STATUS_CODE;
                publisher.StatusDesc = Globals.SUCCESS_TEXT;
                publisher.PublisherName = publisherName;
            }
            catch (Exception ex)
            {
                publisher.StatusCode = Globals.FAILURE_STATUS_CODE;
                publisher.StatusDesc = $"EXCEPTION: {ex.Message}";
            }
            return publisher;
        }

        private static string GetCoverImage(string outPutFilePath, SomaEbook ebook)
        {
            string coverImageID = ReadManifestforCoverImage(outPutFilePath, ebook);
            if (!string.IsNullOrEmpty(coverImageID))
                return coverImageID;
            coverImageID = SomaSharedCommons.ReadChaptersForFirstImage(ebook);
            return coverImageID;
        }


        private static string ReadManifestforCoverImage(string outPutFilePath, SomaEbook ebook)
        {
            string coverImageID = "";
            try
            {

                //for epub..on extraction of zip file
                //there is always a *.opf(e.g content.opf) file that has a manifest that describes location of all content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                //.opf not found
                if (filteredFiles.Length == 0)
                {
                    throw new Exception("NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FORMAT. EPUB MAYBE CORRUPTED");
                }

                //this is the manifest file
                string manifestFileName = filteredFiles[0];

                //Create the XmlDocument.
                XmlDocument doc = new XmlDocument();
                doc.Load(manifestFileName);

                //get all meta tags from the manifest xml document.
                XmlNodeList elemList = doc.GetElementsByTagName("meta");

                //loop thru one by one
                for (int i = 0; i < elemList.Count; i++)
                {
                    //get xml of element
                    string xml = elemList[i].OuterXml;

                    //xml is empty
                    if (string.IsNullOrEmpty(xml))
                    {
                        continue;
                    }

                    //read the xml
                    XmlReader xmlReader = XmlReader.Create(new StringReader(xml));

                    //go to manifest tag
                    bool success = xmlReader.ReadToFollowing("meta");

                    //if its not found
                    if (!success)
                    {
                        continue;
                    }

                    //element doesnt have attributes
                    if (!xmlReader.HasAttributes)
                    {
                        continue;
                    }

                    //look for name attribute
                    string nameAttribute = xmlReader.GetAttribute("name");

                    //no name attribute
                    if (string.IsNullOrEmpty(nameAttribute))
                    {
                        continue;
                    }

                    //is this the cover image
                    if (nameAttribute.ToUpper().Contains("COVER"))
                    {
                        //get its value
                        string coverImageIdref = xmlReader.GetAttribute("content") ?? "";
                        SomaEbookImage ebookImage = ebook.EbookImages.Where(j => j.IdRef.Contains(coverImageIdref)).FirstOrDefault();
                        coverImageID = ebookImage?.ImageID;
                        break;
                    }
                }

                //if we have not found the cover images
                if (string.IsNullOrEmpty(coverImageID))
                {
                    return coverImageID;
                }

                //look for the cover image in memory
                SomaEbookImage img = ebook.EbookImages.Where(i => (i.ImageName == coverImageID)).FirstOrDefault();

                //img not found
                if (img == null)
                {
                    return coverImageID;
                }

                //return the path to the image
                coverImageID = img.ImageID;
            }
            catch (Exception)
            {

            }

            return coverImageID;
        }


        private static List<SomaEbookChapter> ReadChaptersBasedOnManifest(string outPutFilePath, string ebookID, List<SomaEbookImage> ebookImages)
        {
            List<SomaEbookChapter> allChapters = new List<SomaEbookChapter>();

            try
            {
                //for epub..on extraction of zip file
                //there is a file called content.opf that has a manifest that describes location of content
                string[] filteredFiles = Directory.GetFiles(outPutFilePath, "*.opf", SearchOption.AllDirectories);

                if (filteredFiles.Length == 0)
                {
                    throw new Exception("NO EPUB MANIFEST AND SPINE FOUND. INVALID EPUB FORMAT. EPUB MAYBE CORRUPTED");
                }

                string manifestFileName = filteredFiles[0];

                //initialize reader to read manifest xml
                XmlReader reader = XmlReader.Create(manifestFileName);

                //go to manifest tag
                bool success = reader.ReadToFollowing("manifest");

                //if its found
                if (!success)
                {
                    throw new Exception("UNABLE TO READ EPUB: NO MANIFEST FOUND. EPUB MAYBE CORRUPTED");
                }

                int count = 1;

                //loop thru manifest inorder to find the html files
                //those are the chapters
                while (reader.Read())
                {
                    if ((reader.NodeType != XmlNodeType.Element))
                    {
                        continue;
                    }

                    if (reader.Name.ToUpper() != "item".ToUpper())
                    {
                        continue;
                    }

                    if (!reader.HasAttributes)
                    {
                        continue;
                    }

                    //string FilePath to chapter
                    string fileName = reader.GetAttribute("href").Replace("/", Globals.DIR_SEPARATOR);

                    //if its not an html file..probably image
                    //.htm,.html,.xhtml,.xhtm
                    if (!fileName.Contains(".htm")&& !fileName.Contains(".xhtm"))
                    {
                        ebookImages.ForEach(i =>
                        {
                            string actual_filename = fileName.Split(new string[] { Globals.DIR_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()??"";
                            string actual_imgfilename= i.ImageFilePath.Split(new string[] { Globals.DIR_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault() ?? "";
                            if (actual_imgfilename.ToUpper()==actual_filename.ToUpper())
                            {
                                i.IdRef = reader.GetAttribute("id");
                            }
                        });

                        continue;
                    }

                    //look for file with matching name in folder
                    string[] path_splitters = { Globals.DIR_SEPARATOR, "/" };
                    var filter = new string[] { fileName.Split(path_splitters, StringSplitOptions.RemoveEmptyEntries).Last() };
                    var files = SomaSharedCommons.GetFilesMatchingPatternFromDirectory(outPutFilePath, filter, true);

                    if (files.Length <= 0)
                    {
                        continue;
                    }

                    Status read_status = ReadChapterContentFromPath(files.FirstOrDefault(), ebookImages);

                    //something is wrong with the chapter content
                    if (read_status.StatusCode != Globals.SUCCESS_STATUS_CODE)
                    {
                        //throw new Exception($"UNABLE TO PARSE CHAPTER [{count}] CONTENT.");
                        continue;
                    }

                    //create chapter in Ebook
                    SomaEbookChapter chapter = new SomaEbookChapter
                    {
                        ChapterTitle = ParseChapterTitle(read_status.ResultID, reader.GetAttribute("id")),
                        EbookId = ebookID,
                        IdRef = reader.GetAttribute("id"),
                        ChapterId = SharedCommons.GenerateUniqueId("CHAPTER-"),
                        ChapterContent = read_status.ResultID,
                        StatusCode = Globals.SUCCESS_STATUS_CODE,
                        StatusDesc = Globals.SUCCESS_TEXT
                    };

                    //add chapter to e-book
                    allChapters.Add(chapter);

                    //increment chapter count
                    count++;
                }

            }
            catch (Exception ex)
            {
                throw new Exception("UNABLE TO READ EPUB: " + ex.Message);
            }

            return OrderChaptersBasedOnSpine(outPutFilePath, allChapters);
        }




        public static SomaEbook ExtractEbook(string filepath_to_ebook)
        {
            SomaEbook ebook = new SomaEbook();

            if (!filepath_to_ebook.ToUpper().EndsWith(".EPUB"))
            {
                ebook.StatusCode = Globals.FAILURE_STATUS_CODE;
                ebook.StatusDesc = "INVALID FILE FORMAT. PLEASE SUPPLY PATH TO VALID EPUB FILE";
                return ebook;
            }

            ebook = ExtractAndReadEbookContent(filepath_to_ebook);
            return ebook;
        }
    }
}



