﻿using Castle.ActiveRecord;
using DbEntity;
using HtmlAgilityPack;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaEbookChapters")]
    public class SomaEbookChapter : DbEntity<SomaEbookChapter>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 50)]
        public string ChapterId { get; set; }

        [Property(Length = 500)]
        public string ChapterTitle { get; set; }

        [Property(Length = 7000)]
        public string ChapterContent { get; set; }

        [MaxLength(2000)]
        public string FilePath { get; set; }

        [Property]
        public int ChapterIndex_Internal { get; set; }

        [Property]
        public int ChapterIndex_External { get; set; }

        [Property(Length = 50)]
        public string ChapterCoverImageId { get; set; }

        [Property(Length = 50)]
        public string IdRef { get; set; }

        [Property(Length = 50)]
        public string EbookId { get; set; }

        [Property]
        public bool IsTrueChapter { get; set; }

        [Property]
        public int ScrollbarPosition { get; set; }


        public SomaEbookChapter()
        {
            ScrollbarPosition = 5;
        }


        public string ReplaceImagePathsToWebPaths(string imgBasePath)
        {
            Status result = new Status();
            string chapterContent = this.ChapterContent;
            try
            {
                

                if (string.IsNullOrEmpty(chapterContent))
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"NO CHAPTER CONTENT FOUND. LOAD CONTENT INTO THE CHAPTER CONTENT FIELD";
                    return chapterContent;
                }

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(chapterContent);
                var nodes = doc.DocumentNode.SelectNodes("//img[@src]");

                //no img tags found in the html
                if (nodes == null)
                {
                    return chapterContent;
                }


                foreach (HtmlNode node in nodes)
                {
                    Console.WriteLine($"ImgBasePath Before: {imgBasePath}");
                    imgBasePath = $"{imgBasePath}/GetImage";

                    string imgSource_atrribute = node.Attributes["src"].Value;
                    string[] parts = imgSource_atrribute.Split('/');
                    imgSource_atrribute = $"{imgBasePath}/{parts.Last()}";
                    Console.WriteLine($"ImgBasePath After: {imgBasePath}");

                    //change the src attribute of the image
                    node.SetAttributeValue("src", $"{imgSource_atrribute}");
                    continue;


                }

                var content = doc.DocumentNode.OuterHtml;
                return content;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return chapterContent;
        }

    }

}
