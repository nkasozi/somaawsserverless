﻿using Castle.ActiveRecord;
using DbEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaEbookAuthors")]
    public class SomaEbookAuthor : DbEntity<SomaEbookAuthor>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 500)]
        public string AuthorName { get; set; }

        [Property(Length = 500, Unique = true)]
        public string AuthorID { get; set; }
    }
}
