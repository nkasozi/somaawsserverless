﻿using Castle.ActiveRecord;
using DbEntity;
using SomaEbookManager.HelperClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaEbooks")]
    public class SomaEbook : DbEntity<SomaEbook>
    {

        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 50)]
        public string EbookId { get; set; }

        [Property(Length = 50)]
        public string Title { get; set; }

        [Property(Length = 50)]
        public string FilePath { get; set; }

        [Property(Length = 50)]
        public string EbookTypeID { get; set; }

        [Property(Length = 50)]
        public string PublisherID { get; set; }

        [Property(Length = 50)]
        public string AuthorID { get; set; }

        [Property(Length = 50)]
        public string OwnerID { get; set; }

        [Property(Length = 50)]
        public string CoverImageID { get; set; }

        [Property]
        public int CurrentChapterIndex { get; set; }

        [Property(Length = 50)]
        public string CurrentReaderID { get; set; }

        [Property]
        public bool IsLentOut { get; set; }

        [Property]
        public DateTime LentOutOn { get; set; }

        [Property]
        public double HoursLentOut { get; set; }

        [Property]
        public DateTime UploadDate { get; set; }

        [Property]
        public DateTime ModifiedDate { get; set; }

        [Property(Length = 600,Unique = true)]
        public string EbookHash { get; set; }

        public List<SomaEbookImage> EbookImages { get; set; }

        public List<SomaEbookChapter> EbookChapters { get; set; }

        public SomaEbookAuthor Author { get; set; }

        public SomaEbookPublisher Publisher { get; set; }

        public SomaEbookType EbookType { get; set; }

        public SomaEbook()
        {
            LentOutOn = DateTime.Now;
            UploadDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
        }

        public override bool IsValid()
        {
            if (EbookChapters.Count <= 0)
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = "No Chapters found in Ebook";
                return false;
            }
            return base.IsValid();
        }



    }
}
