﻿namespace SomaEbookManager
{
    public class Status
    {
        //public string Status { get; set; }

        public string ResultID { get; set; }

        public string StatusCode { get; set; }

        public string StatusDesc { get; set; }

        public Status()
        {
            //Status = "";
            StatusCode = "";
            StatusDesc = "";
        }
    }
}

