﻿using Castle.ActiveRecord;
using DbEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaEbookPublishers")]
    public class SomaEbookPublisher : DbEntity<SomaEbookPublisher>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 500)]
        public string PublisherName { get; set; }

        [Property(Length = 50, Unique = true)]
        public string PublisherID { get; set; }
    }
}
