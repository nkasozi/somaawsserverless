﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    public class LendRequest:Status
    {
        public string CurrentOwnerID { get; set; }
        public string NewReaderID { get; set; }
        public string EbookID { get; set; }
        public string CurrentOwnerPassword { get; set; }
        public string DurationInHours { get; set; }

        public bool IsValidLendOutRequest()
        {
            bool is_valid_int = int.TryParse(DurationInHours, out int duration_in_hours);

            if (!is_valid_int)
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = $"PLEASE SUPPLY A VALID LEND DURATION [{DurationInHours}]";
                return false;
            }

            if (string.IsNullOrEmpty(CurrentOwnerID))
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = $"PLEASE SUPPLY A CURRENT OWNER ID";
                return false;
            }

            if (string.IsNullOrEmpty(EbookID))
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = $"PLEASE SUPPLY AN EBOOK ID";
                return false;
            }

            if (string.IsNullOrEmpty(NewReaderID))
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = $"PLEASE SUPPLY THE NEW OWNER's/READER's ID";
                return false;
            }

            return true;
        }
    }
}
