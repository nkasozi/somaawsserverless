﻿using Castle.ActiveRecord;
using DbEntity;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaSystemUsers")]
    public class SomaSystemUser : DbEntity<SomaSystemUser>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 50, Unique = true)]
        public string Username { get; set; }

        [Property(Length = 50)]
        public string Email { get; set; }

        [Property(Length = 50)]
        public string Password { get; set; }

        public override bool IsValid()
        {
            if (string.IsNullOrEmpty(Username))
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = "PLEASE SUPPLY A USERNAME/EMAIL";
                return false;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                StatusCode = Globals.FAILURE_STATUS_CODE;
                StatusDesc = "PLEASE SUPPLY A PASSWORD";
                return false;
            }

            StatusCode = Globals.SUCCESS_STATUS_CODE;
            return true;
        }


    }
}
