﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    public class SomaReport:Status
    {
        [NotMapped]
        public DataTable ReportData { get; set; }

        [NotMapped]
        public string ReportName { get; set; }
    }
}
