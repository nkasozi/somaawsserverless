﻿using Castle.ActiveRecord;
using DbEntity;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaImages")]
    public class SomaEbookImage : DbEntity<SomaEbookImage>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Required]
        [Property(Length = 150)]
        public string ImageID { get; set; }

        [Property(Length = 8000)]
        public byte[] Image { get; set; }

        [Required]
        [Property(Length = 500)]
        public string ImageName { get; set; }

        [Property(Length = 50)]
        public string IdRef { get; set; }

        [Required]
        [Property(Length = 3000)]
        public string ImageFilePath { get; set; }

        [Property(Length = 3000)]
        public string WebFilePath { get; set; }

        [Required]
        [Property(Length = 50)]
        public string EbookId { get; set; }

        public SomaEbookImage()
        {

        }

        public SomaEbookImage(string image_name, string path_to_image, string ebook_id)
        {
            ImageName = image_name;
            ImageFilePath = path_to_image;
            EbookId = ebook_id;
            ImageID = "IMG-"+Guid.NewGuid().ToString();
            WebFilePath = $"Images/GetImage/{ImageID}";
            Image = File.ReadAllBytes(ImageFilePath);
        }

        public static Status ReplaceImagePathsToWebPaths(string chapterContent, List<SomaEbookImage> ebookImages)
        {
            Status result = new Status();
            try
            {
                if (string.IsNullOrEmpty(chapterContent))
                {
                    result.StatusCode = Globals.FAILURE_STATUS_CODE;
                    result.StatusDesc = $"NO CHAPTER CONTENT FOUND. LOAD CONTENT INTO THE CHAPTER CONTENT FIELD";
                    return result;
                }

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(chapterContent);
                var nodes = doc.DocumentNode.SelectNodes("//img[@src]");

                if (nodes == null)
                {
                    result.ResultID = chapterContent;
                    result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    result.StatusDesc = Globals.SUCCESS_TEXT;
                    return result;
                }


                foreach (HtmlNode node in nodes)
                {
                    var img_src = node.Attributes["src"].Value;
                    img_src = img_src.Replace("/", Globals.DIR_SEPARATOR);

                    //find image for ebook chapter
                    SomaEbookImage img = ebookImages.Where(i => ((i.ImageFilePath.ToUpper().Contains(img_src.ToUpper())))).FirstOrDefault();

                    if (img != null)
                    {
                        node.SetAttributeValue("src", $"{img.WebFilePath}");
                    }

                }

                var content = doc.DocumentNode.OuterHtml;
                content = SomaSharedCommons.RemoveUnwantedHtmlTagsButKeepChildTags(content, new List<string>() { "svg" });
                result.ResultID = content;
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"EXCEPTION:{ex.Message}";
            }
            return result;
        }



        public Status SaveAndUploadImage()
        {
            Status status = new Status();
            try
            {
                //read the image bytes
                Image = File.ReadAllBytes(ImageFilePath);

                if (!IsValid())
                {
                    status.StatusCode = Globals.FAILURE_STATUS_CODE;
                    status.StatusDesc = this.StatusCode;
                    return status;
                }

                //Save and Upload Image Asynchronously
                Task<int> save_into_db_task = SaveWithStoredProcAutoParamsAsync("SaveEbookImage");
                Task<SomaEbookImage> upload_image_task = Task.Factory.StartNew(() => EbookImageHandler.UploadImage(this));

                //wait for all of them to finish
                Task.WaitAll(save_into_db_task, upload_image_task);
                WebFilePath = upload_image_task.Result.WebFilePath;

                //success
                status.StatusCode = Globals.SUCCESS_STATUS_CODE;
                status.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                status.StatusCode = Globals.FAILURE_STATUS_CODE;
                status.StatusDesc = $"ERROR:{ex.Message}";
            }
            return status;
        }

        public override bool IsValid()
        {
            if (CheckIfAllRequiredAttributesAreSet().StatusCode != Globals.SUCCESS_STATUS_CODE)
            {
                return false;
            }
            return base.IsValid();
        }
    }

}