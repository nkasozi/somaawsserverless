﻿using Castle.ActiveRecord;
using DbEntity;
using System;
using System.Linq;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SessionTokens")]
    public class SessionToken : DbEntity<SessionToken>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 50)]
        public string UserId { get; set; }

        [Property(Length = 500)]
        public string TokenValue { get; set; }

        [Property]
        public DateTime CreatedAt { get; set; }

        public static SomaSystemUser VerifyUserToken(string token)
        {
            SomaSystemUser user = new SomaSystemUser();
            try
            {
                    SessionToken sessionToken = SessionToken.QueryWithStoredProc("GetSessionTokenByID",token).FirstOrDefault();

                    //token not found
                    if (sessionToken == null)
                    {
                        user.StatusCode = Globals.FAILURE_STATUS_CODE;
                        user.StatusDesc = $"SESSION TOKEN NOT FOUND";
                        return user;
                    }

                    user.Username = sessionToken.UserId;
                    user.StatusCode = Globals.SUCCESS_STATUS_CODE;
                    user.StatusDesc = Globals.SUCCESS_TEXT;
            
            }
            catch (Exception ex)
            {
                user.StatusCode = Globals.FAILURE_STATUS_CODE;
                user.StatusDesc = $"EXCEPTON: " + ex.Message;
            }
            return user;
        }

        public static SessionToken GenerateToken(string userId)
        {
            SessionToken token = new SessionToken();
            try
            {
                token.UserId = userId;
                token.TokenValue = Guid.NewGuid().ToString();
                token.CreatedAt = DateTime.Now;

                token.Save();

                token.StatusCode = Globals.SUCCESS_STATUS_CODE;
                token.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch (Exception ex)
            {
                token.StatusCode = Globals.FAILURE_STATUS_CODE;
                token.StatusDesc = $"EXCEPTON: " + ex.Message;
            }
            return token;
        }
    }
}
