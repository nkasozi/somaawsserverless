﻿using Castle.ActiveRecord;
using DbEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    [ActiveRecord("SomaAuditLogs")]
    public class SomaAuditLog : DbEntity<SomaAuditLog>
    {
        [PrimaryKey(PrimaryKeyType.Identity, "RecordId")]
        public int Id { get; set; }

        [Property(Length = 50)]
        public string UserId { get; set; }

        [Property(Length = 50)]
        public string ActivityTypeCode { get; set; }

        [Property(Length = 50)]
        public string ActivityDescription { get; set; }

        [Property(Length = 50)]
        public DateTime ActivityDateTime { get; set; }
    }
}
