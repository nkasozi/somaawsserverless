﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomaEbookManager.EntityClasses
{
    public enum SomaEbookType
    {
        EPUB = 1,
        PDF = 2,
        UNSUPPORTED = 3
    }

    public enum AuditLogType
    {
        LOGIN=1,
        LOGOUT = 2,
        UPDATE = 3,
        DELETE = 4,
        CREATE = 5,
        SAVE = 6,
        READ = 7,
        LEND_ACTION=8
    }
}
