﻿using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using SomaEbookManager.ControlClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static Globals;

namespace SomaEbookManager.EntityClasses
{
    public class SomaEpubEbook : SomaEbook
    {
        protected SomaEpubEbook()
        {

        }

        public SomaEpubEbook(string filePath, SomaSystemUser user) : base(filePath, user)
        {
        }

     
}
