﻿using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using SomaEbookManager.EntityClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SomaEbookManager
{
    public static class SomaSharedCommons
    {
        public static string GeneratePassword()
        {
            string Password = "T3rr1613";
            return Password;
        }

        public static String[] GetFilesMatchingPatternFromDirectory(String searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, filter, searchOption));
            }
            return filesFound.ToArray();
        }

        public static string GetDirectoryOfFilePath(string exePath)
        {
            string directory_name = new FileInfo(exePath).DirectoryName;
            return directory_name + "\\";
        }

        public static string FirstWords(string input, int numberWords)
        {
            try
            {
                //to get words in input
                //split input based on whitespace ' ', remove empty strings
                string[] words = input.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string first_x_words = "";

                // Loop through entire input.
                foreach (var word in words)
                {
                    //word is just whitespace
                    if (string.IsNullOrWhiteSpace(word))
                        continue;

                    //we have the first x words
                    //we stop loopping
                    if (numberWords == 0)
                        break;

                    // If we have no more words to display, return the substring.
                    first_x_words += $"{word} ";
                    numberWords--;
                }

                return first_x_words.Trim();
            }
            catch (Exception)
            {
                // Log the error.
            }
            return string.Empty;
        }

        public static string RemoveSpecialCharacters(string sentence)
        {
            if (string.IsNullOrEmpty(sentence))
                return sentence;

            string[] words_in_sentence = sentence.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            string santized_sentence = "";

            // Loop through entire input.
            foreach (var word in words_in_sentence)
            {
                //word is just whitespace
                if (string.IsNullOrWhiteSpace(word))
                    continue;

                string sanitized_word = Regex.Replace(word, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);

                // If we have no more words to display, return the substring.
                santized_sentence += $"{sanitized_word} ";
            }

            return santized_sentence.Trim();
        }

        public static string GenerateHashForFile(string filepath_to_ebook)
        {
            using (FileStream fs = File.OpenRead(filepath_to_ebook))
            using (HashAlgorithm hashAlgorithm = MD5.Create())
            {
                byte[] hash = hashAlgorithm.ComputeHash(fs);
                string result=SharedCommons.ByteToString(hash);
                return result;
            }
            
        }

        public static Status CreateFilePathIfNotExists(string fileFullPath)
        {
            Status result = new Status();
            try
            {
                FileInfo fileInfo = new FileInfo(fileFullPath);
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                result.StatusCode = Globals.SUCCESS_STATUS_CODE;
                result.StatusDesc = Globals.SUCCESS_TEXT;
            }
            catch(Exception ex)
            {
                result.StatusCode = Globals.FAILURE_STATUS_CODE;
                result.StatusDesc = $"ERROR: {ex.Message}";
            }
            
            return result;
        }

        public static bool ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);

                // AES encrypted entries are handled automatically
                if (!string.IsNullOrEmpty(password))
                {
                    zf.Password = password;
                }

                foreach (ZipEntry zipEntry in zf)
                {
                    // Ignore directories
                    if (!zipEntry.IsFile)
                    {
                        continue;
                    }

                    String entryFileName = zipEntry.Name;

                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    if (File.Exists(fullZipToPath))
                    {
                        FileStream streamWriter = new FileStream(fullZipToPath, FileMode.Open, FileAccess.Read);
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                    else
                    {
                        using (FileStream streamWriter = File.Create(fullZipToPath))
                        {
                            StreamUtils.Copy(zipStream, streamWriter, buffer);
                        }
                    }

                    zipStream.Close();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }

        public static SomaEbookType GetEbookType(string filepath_to_ebook)
        {
            if (string.IsNullOrEmpty(filepath_to_ebook))
                return SomaEbookType.UNSUPPORTED;

            string file_extension = Path.GetExtension(filepath_to_ebook);

            if (file_extension.ToUpper() == ".PDF")
                return SomaEbookType.PDF;

            if (file_extension.ToUpper() == ".EPUB")
                return SomaEbookType.EPUB;

            return SomaEbookType.UNSUPPORTED;
        }

        public static bool DeleteOutputFolder(string filepath_to_directory)
        {
            try
            {
                Directory.Delete(filepath_to_directory);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes);
        }

        public static string HashPassword(string password)
        {
            return ComputeHash(password, HashAlgorithm.Create("MD5"));
        }

        public static string RemoveUnwantedHtmlTagsButKeepChildTags(this string html, List<string> unwantedTags)
        {
            if (String.IsNullOrEmpty(html))
            {
                return html;
            }

            var document = new HtmlDocument();
            document.LoadHtml(html);

            HtmlNodeCollection tryGetNodes = document.DocumentNode.SelectNodes("./*|./text()");

            if (tryGetNodes == null || !tryGetNodes.Any())
            {
                return html;
            }

            var nodes = new Queue<HtmlNode>(tryGetNodes);

            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                var childNodes = node.SelectNodes("./*|./text()");

                if (childNodes != null)
                {
                    foreach (var child in childNodes)
                    {
                        nodes.Enqueue(child);
                    }
                }

                if (unwantedTags.Any(tag => tag == node.Name))
                {
                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            parentNode.InsertBefore(child, node);
                        }
                    }

                    parentNode.RemoveChild(node);
                }
            }

            return document.DocumentNode.InnerHtml;
        }

        public static string ReadChaptersForFirstImage(SomaEbook ebook)
        {
            string coverImageID = "";
            try
            {
                foreach (var chapter in ebook.EbookChapters)
                {
                    string chapterContent = chapter.ChapterContent;

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(chapterContent);
                    var nodes = doc.DocumentNode.SelectNodes("//img[@src]");

                    if (nodes == null)
                    {
                        continue;
                    }

                    foreach (HtmlNode node in nodes)
                    {
                        var imgSource_atrribute = node.Attributes["src"].Value;
                        imgSource_atrribute = imgSource_atrribute.Replace("/", Globals.DIR_SEPARATOR);
                        imgSource_atrribute = imgSource_atrribute.Split(new string[] { Globals.DIR_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

                        //find image for ebook chapter
                        SomaEbookImage img = ebook.EbookImages.Where(i => ((i.ImageFilePath.ToUpper().Contains(imgSource_atrribute?.ToUpper())))).FirstOrDefault();

                        if (img != null)
                        {
                            //change the src attribute of the image
                            return img.ImageID;
                        }

                        //find image for ebook chapter
                        img = ebook.EbookImages.Where(i => ((imgSource_atrribute.Contains(i.ImageID)))).FirstOrDefault();

                        if (img != null)
                        {
                            //change the src attribute of the image
                            return img.ImageID;
                        }

                    }
                }
            }
            catch (Exception)
            {
            }
            return coverImageID;
        }



        public static void SeedSystemUsers()
        {
            SomaSystemUser user = new SomaSystemUser()
            {
                Username = "Nsubugak",
                Password = GeneratePassword()
            };

            user.Save();
        }
    }

}