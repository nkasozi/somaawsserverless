﻿using DbEntity;
using SomaEbookManager.EntityClasses;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;

namespace SomaEbookManager
{
    public class SomaDbInitializer
    {
        public static Status Initiliaze(string ConnectionString = null)
        {
            Status status = new Status();
            try
            {

                DbResult dbResult = DbInitializer.LoadTypesToKeepTrackOfFromAssembly(Assembly.GetAssembly(typeof(SomaDbInitializer)));
                DbResult result = DbInitializer.Initialize(ConnectionString);

                if (result.StatusCode != DbGlobals.SUCCESS_STATUS_CODE)
                {
                    status.StatusCode = result.StatusCode;
                    status.StatusDesc = result.StatusDesc;
                    return status;
                }

                Seed();
                status.StatusCode = result.StatusCode;
                status.StatusDesc = result.StatusDesc;
            }
            catch (System.Exception ex)
            {
                status.StatusCode = Globals.FAILURE_STATUS_CODE;
                status.StatusDesc = ex.Message;
            }
            return status;
        }

        public static byte[] BitmapToByteArray(Bitmap bitmap)
        {
            byte[] data;
            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Png);
                data = ms.ToArray();
            }
            return data;
        }

        public static byte[] DownloadImage(string imageUrl)
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(imageUrl);
            Bitmap bitmap = new Bitmap(stream);

            if (bitmap == null)
            {
                throw new Exception("Unable to Seed Ebook Images");
            }

            byte[] result = BitmapToByteArray(bitmap);
            stream.Flush();
            stream.Close();
            client.Dispose();
            return result;
        }

        private static Status Seed()
        {
            //seed db stuff here
            SomaSystemUser user = new SomaSystemUser();
            user.Username = "Nsubugak";
            user.Password = "T3rr1613";
            user.Email = "nsubugak@yahoo.com";
            SomaEbookManagerAPI soma = new SomaEbookManagerAPI();
            Status result = soma.SaveSystemUser(user);

            SomaEbookImage dbImage = SomaEbookImage.QueryWithStoredProc("GetImageById", "not_found").FirstOrDefault();

            if (dbImage == null)
            {
                SeedImages();
            }



            return result;


        }

        private static void SeedImages()
        {
            SomaEbookImage image = new SomaEbookImage();

            image.WebFilePath = "http://placehold.it/320x240/E8117F/FFFFFF?text=N/A";
            image.ImageFilePath = image.WebFilePath;
            image.Image = DownloadImage(image.WebFilePath);
            image.ImageName = "PlaceHolder.png";
            image.EbookId = "NOT_FOUND";
            image.ImageID = "NOT_FOUND";
            image.StatusCode = Globals.SUCCESS_STATUS_CODE;
            image.StatusDesc = Globals.SUCCESS_TEXT;
            image.SaveWithStoredProcAutoParamsAsync("SaveImage");
        }
    }
}
