﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


public static class StringExtensionMethods
{
    public static string ReplaceFirst(this string text, string oldText, string newText)
    {
        var regex = new Regex(Regex.Escape(oldText));
        var thenewText = regex.Replace(text, newText, 1);
        return thenewText;
    }
}

